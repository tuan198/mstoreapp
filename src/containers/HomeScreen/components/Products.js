import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import colors from '../../../assets/styles/colors';
import Bullets from 'react-native-easy-content-loader';
import productActions from '../../../redux/product/actions';
import {useDispatch, useSelector} from 'react-redux';

export default function Product({navigation}) {
  const dispatch = useDispatch();
  const {products, productLoading} = useSelector(state => state.Product);

  useEffect(() => {
    dispatch(productActions.getProducts());
  }, [dispatch]);

  return (
    <View>
      <Bullets loading={productLoading} active listSize={2} pWidth={['100%']}>
        <FlatList
          numColumns={2}
          showsHorizontalScrollIndicator={false}
          data={products}
          keyExtractor={(item, index) => index + item}
          horizontal={false}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('HomeDetail', {
                  id: item.id,
                  name: item.name,
                  price: item.price,
                  image: item.image,
                })
              }
              style={[Styles.productList, Styles.lastProductList]}>
              <ImageBackground
                source={{uri: item.image}}
                style={Styles.lastProductListImage}
                imageStyle={Styles.imageStyle}
              />
              <Text style={Styles.featureItemName}>{item.name}</Text>
              <Text style={Styles.featureItemPrice}>{item.price} ₫</Text>
            </TouchableOpacity>
          )}
        />
      </Bullets>
    </View>
  );
}

const Styles = StyleSheet.create({
  productList: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 8,
  },
  lastProductList: {
    flexDirection: 'column',
    width: '46%',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },
  lastProductListImage: {
    height: 140,
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  imageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  featureItemName: {
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 3,
  },
  featureItemPrice: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.secondary,
    marginBottom: 5,
  },
});

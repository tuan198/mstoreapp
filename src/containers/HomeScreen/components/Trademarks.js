import React, {useState, useEffect} from 'react';
import {
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Image,
  Text,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import colors from '../../../assets/styles/colors';
import Bullets from 'react-native-easy-content-loader';
import trademarkActions from '../../../redux/trademark/actions';
import {useDispatch, useSelector} from 'react-redux';

export default function({navigation}) {
  const dispatch = useDispatch();
  const {trademarks, trademarkLoading} = useSelector(state => state.Trademark);

  useEffect(() => {
    dispatch(trademarkActions.getTrademarks());
  }, [dispatch]);

  return (
    <Bullets
      loading={trademarkLoading}
      active
      listSize={1}
      pHeight={[25]}
      pWidth={['100%']}>
      <FlatList
        showsHorizontalScrollIndicator={false}
        data={trademarks}
        keyExtractor={(item, index) => index + item}
        horizontal={true}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('ProductList', {
                id: item.id,
                name: item.name,
                collection: 'trademarks',
              })
            }
            style={Styles.categoryItem}>
            <Image source={{uri: item.image}} style={Styles.categoryImage} />
            <Text style={Styles.categoryTitle}>{item.name}</Text>
          </TouchableOpacity>
        )}
      />
    </Bullets>
  );
}

const Styles = StyleSheet.create({
  categoryItem: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 16,
    backgroundColor: 'white',
  },
  categoryImage: {
    height: 80,
    width: 80,
    borderRadius: 100,
    borderColor: colors.eggBlue,
    borderWidth: 1,
  },
  categoryTitle: {
    textTransform: 'uppercase',
  },
});

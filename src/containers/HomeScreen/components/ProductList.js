import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import colors from '../../../assets/styles/colors';
import ContentLoader from 'react-native-easy-content-loader';
import productActions from '../../../redux/product/actions';
import categoryActions from '../../../redux/category/actions';
import trademarkActions from '../../../redux/trademark/actions';
import {useDispatch, useSelector} from 'react-redux';

export default function ProductList({navigation, route}) {
  const {id, name, collection} = route.params;
  const dispatch = useDispatch();
  const {
    Category: {categoryList, categoryLoading},
    Product: {productList, productLoading},
    Trademark: {trademarkList, trademarkLoading},
  } = useSelector(state => state);
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (id === 'Product' && collection === 'categories') {
      dispatch(productActions.getProductList());
    } else if (collection === 'trademarks') {
      dispatch(trademarkActions.getTrademarkList({trademark: id}));
    } else {
      dispatch(categoryActions.getCategoryList({category: id}));
    }
  }, [collection, dispatch, id]);

  useEffect(() => {
    if (id === 'Product' && collection === 'categories') {
      setData(productList);
      setLoading(productLoading);
    } else if (collection === 'trademarks') {
      setData(trademarkList);
      setLoading(trademarkLoading);
    } else {
      setData(categoryList);
      setLoading(categoryLoading);
    }
  }, [
    id,
    collection,
    productList,
    trademarkList,
    categoryList,
    productLoading,
    trademarkLoading,
    categoryLoading,
  ]);

  // useEffect(() => {
  //   if (categoryList || productList || trademarkList) {
  //     setLoading(false);
  //   }
  // }, [categoryList, productList, trademarkList]);

  return (
    <View>
      <View style={Styles.container}>
        <Text style={Styles.title}>{name}</Text>
      </View>
      <View style={loading ? {paddingTop: 50} : null}>
        <ContentLoader
          loading={loading}
          pRows={10}
          pHeight={[200, 30, 20]}
          pWidth={[200, 70, 100]}>
          <View style={Styles.List}>
            <FlatList
              numColumns={2}
              showsHorizontalScrollIndicator={false}
              data={data}
              keyExtractor={(item, index) => index + item}
              horizontal={false}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() =>
                    navigation.navigate('HomeDetail', {
                      id: item.id,
                    })
                  }
                  style={[Styles.productList, Styles.lastProductList]}>
                  <ImageBackground
                    source={{uri: item.image}}
                    style={Styles.lastProductListImage}
                    imageStyle={Styles.imageStyle}
                  />
                  <Text style={Styles.featureItemName}>{item.name}</Text>
                  <Text style={Styles.featureItemPrice}>{item.price} ₫</Text>
                </TouchableOpacity>
              )}
            />
          </View>
        </ContentLoader>
      </View>
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 80,
    paddingLeft: 24,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.eggBlue,
  },
  List: {
    marginTop: 10,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: colors.white,
    marginBottom: 160,
  },
  lastProductListImage: {
    height: 140,
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  imageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  featureItemName: {
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 3,
  },
  featureItemPrice: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.secondary,
    marginBottom: 5,
  },
  productList: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 8,
  },

  lastProductList: {
    flexDirection: 'column',
    width: '46%',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },
});

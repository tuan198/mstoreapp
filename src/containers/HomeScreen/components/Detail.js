import React, {useEffect, useState, useRef} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Button,
  ScrollView,
} from 'react-native';
import {Col, Grid} from 'react-native-easy-grid';
import firestore from '@react-native-firebase/firestore';
import {SliderBox} from 'react-native-image-slider-box';
import Bullets from 'react-native-easy-content-loader';
import colors from '../../../assets/styles/colors';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import auth from '@react-native-firebase/auth';

export default function HomeDetail({navigation, route}) {
  const {id} = route.params;
  const [product, setProduct] = useState([]);
  const [loading, setLoading] = useState(true);
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const refRBSheet = useRef();
  const refRBSheetCart = useRef();
  const [quantum, setQuantum] = useState(1);

  // eslint-disable-next-line react-hooks/exhaustive-deps,no-shadow
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  useEffect(() => {
    let Product = [];
    firestore()
      .collection('products')
      .doc(id)
      .get()
      .then(documentSnapshot => {
        const data = documentSnapshot.data();
        // console.log('User exists: ', documentSnapshot.data());
        // if (documentSnapshot.exists) {
        Product.push({id: documentSnapshot.id, ...data});
        Product.push(
          data.image1,
          data.image2,
          data.image3,
          data.image4,
          data.image5,
        );
      })
      .then(() => setProduct(...Product))
      .then(() => setLoading(false));
  }, [id]);

  useEffect(() => {
    if (user) {
      const userId = auth().currentUser.uid;
      firestore()
        .collection('shopping-card')
        .doc(userId)
        .collection('products')
        .doc(product.id)
        .get()
        .then(doc => {
          const quan = doc.data().quantum;
          setQuantum(quan);
        });
    }
  });

  if (initializing) {
    return null;
  }

  const addToCard = () => {
    if (user) {
      const userId = auth().currentUser.uid;

      firestore()
        .collection('shopping-card')
        .doc(userId)
        .collection('products')
        .doc(product.id)
        .get()
        .then(doc => {
          const quanum = doc.data().quantum;
          setQuantum(quanum);
          let ref = firestore()
            .collection('shopping-card')
            .doc(userId)
            .collection('products')
            .doc(product.id);
          ref
            .set({
              ...product,
              quantum: quanum + 1,
            })
            .then(() => refRBSheetCart.current.open())
            .catch(e => console.log(e));
        })
        .catch(() => {
          let ref = firestore()
            .collection('shopping-card')
            .doc(userId)
            .collection('products')
            .doc(product.id);
          ref
            .set({
              ...product,
              quantum: 1,
            })
            .then(() => refRBSheetCart.current.open())
            .catch(e => console.log(e));
        });
    } else {
      navigation.navigate('BillingInfo', {
        id: product.id,
      });
    }
  };

  return (
    <Bullets
      loading={loading}
      active
      listSize={6}
      pHeight={[25]}
      pWidth={['100%']}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <SliderBox
            sliderBoxHeight={300}
            dotColor="#FFEE58"
            inactiveDotColor="#90A4AE"
            autoplay
            circleLoop
            disableOnPress
            images={product.images}
          />
          <View style={Styles.detailContain}>
            <Text style={Styles.name}>{product.name}</Text>
            <View style={Styles.groupPrice}>
              <Text style={Styles.price}>{product.price} ₫</Text>
              {product.oldPrice ? (
                <Text style={Styles.oldPrice}>{product.oldPrice} ₫</Text>
              ) : null}
            </View>
            {product.amount === quantum ? (
              <Text>Bạn đã chọn hết sản phẩm trong kho</Text>
            ) : (
              <Text>Còn {product.amount} sản phẩm</Text>
            )}
            <View style={Styles.itemCenter}>
              <TouchableOpacity
                disabled={product.amount === quantum}
                style={
                  product.amount === quantum
                    ? Styles.btnBuyDisable
                    : Styles.btnBuy
                }
                onPress={() => {
                  user ? addToCard() : navigation.navigate('SignIn');
                }}>
                <Text style={Styles.btnBuyTitle}>Chọn mua</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={[Styles.detailContain, Styles.detailContainSecond]}>
            <Grid>
              <Col style={Styles.colStyle}>
                <MaterialCommunityIcons
                  name="shield-check-outline"
                  size={40}
                  color="blue"
                />
                <Text style={Styles.fz18}>Bồi thường</Text>
                <Text style={[Styles.fz18, Styles.fwBold]}>111%</Text>
                <Text style={Styles.fz18}>nếu hàng giả</Text>
              </Col>
              <Col style={Styles.colStyle}>
                <MaterialCommunityIcons
                  name="thumb-up-outline"
                  size={40}
                  color="blue"
                />
                <Text style={Styles.fz18}>Thông tin</Text>
                <Text style={Styles.fz18}>bảo hành</Text>
                <Text
                  style={[Styles.fz18, Styles.clBlue]}
                  onPress={() => refRBSheet.current.open()}>
                  XEM CHI TIẾT
                </Text>
              </Col>
              <Col style={Styles.colStyle}>
                <MaterialCommunityIcons
                  name="rewind-30"
                  size={40}
                  color="blue"
                />
                <Text style={Styles.fz18}>Đổi trả trong</Text>
                <Text style={[Styles.fz18, Styles.fwBold]}>30 ngày</Text>
                <Text style={Styles.fz18}>nếu sp lỗi</Text>
              </Col>
            </Grid>
          </View>
          {product.category !== 'Accessories' && (
            <View style={[Styles.detailContain, {marginTop: 10}]}>
              <Text style={[Styles.name, {fontWeight: 'bold'}]}>
                Thông tin chi tiết
              </Text>
              <View style={{marginTop: 20}}>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Bộ xử lý
                  </Text>
                  <Text style={{width: '65%', alignSelf: 'flex-end'}}>
                    {product.cpuType}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text
                    style={{
                      width: '65%',
                      alignItems: 'flex-end',
                      marginBottom: 10,
                    }}>
                    {product.cpuInfo}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>RAM</Text>
                  <Text style={{width: '65%'}}>{product.ramMemory}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.ramInfo}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>Ổ cứng</Text>
                  <Text style={{width: '65%'}}>{product.hardDisk}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.hardDiskInfo}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Màn hình
                  </Text>
                  <Text style={{width: '65%'}}>{product.screenSize}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%'}}>{product.resolution}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.screenTechnology}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Card màn hình
                  </Text>
                  <Text style={{width: '65%'}}>{product.cardDesign}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%'}}>{product.memoryCard}</Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}} />
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.graphicCard}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>Pin</Text>
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.pinInfo}
                  </Text>
                </View>

                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Âm thanh
                  </Text>
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.audioTechnology}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Hệ điều hành
                  </Text>
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.operatingSystem}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Kích thước
                  </Text>
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.size}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Trọng lượng
                  </Text>
                  <Text style={{width: '65%', marginBottom: 10}}>
                    {product.weight}
                  </Text>
                </View>
                <View style={Styles.detailTable}>
                  <Text style={{width: '35%', fontWeight: 'bold'}}>
                    Cổng giao tiếp
                  </Text>
                  <Text style={{width: '65%'}}>{product.port}</Text>
                </View>
              </View>
            </View>
          )}
          <View style={[Styles.detailContain, {marginTop: 10}]}>
            <Text style={[Styles.name, {fontWeight: 'bold'}]}>
              Mô tả sản phẩm
            </Text>
            <Text style={{fontSize: 16, marginTop: 10}}>
              {product.description}
            </Text>
          </View>
          <RBSheet
            ref={refRBSheet}
            closeOnDragDown={true}
            closeOnPressMask={true}
            animationType="fade"
            height={200}
            customStyles={CustomStyle}>
            <View
              style={{
                margin: 15,
                justifyContent: 'space-between',
                flex: 1,
                flexDirection: 'column',
              }}>
              <View>
                <Text
                  style={{fontSize: 24, fontWeight: 'bold', marginBottom: 10}}>
                  Thông tin bảo hành
                </Text>
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>Nhà cung cấp</Text>
                  <Text style={{fontSize: 16}}>{product.trademark}</Text>
                </View>
                <View
                  style={{
                    justifyContent: 'space-between',
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Text style={{fontSize: 16}}>Thời gian bảo hành</Text>
                  <Text style={{fontSize: 16}}>{product.guarantee}</Text>
                </View>
              </View>
              <Button
                title="Tôi đã hiểu"
                onPress={() => refRBSheet.current.close()}
              />
            </View>
          </RBSheet>
          <RBSheet
            ref={refRBSheetCart}
            closeOnDragDown={true}
            closeOnPressMask={true}
            animationType="fade"
            height={250}
            customStyles={CustomStyle}>
            <View
              style={{
                margin: 15,
                justifyContent: 'space-between',
                flex: 1,
                flexDirection: 'column',
              }}>
              <View>
                <Text
                  style={{fontSize: 24, fontWeight: 'bold', marginBottom: 10}}>
                  Thêm sản phẩm
                </Text>
                <View>
                  <Text style={{fontSize: 16, marginBottom: 10}}>
                    Thêm thành công sản phẩm vào giỏ hàng
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: colors.blue,
                      fontWeight: 'bold',
                    }}>
                    Tên sản phẩm: {product.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 16,
                      color: colors.google,
                      fontWeight: 'bold',
                    }}>
                    Giá: {product.price}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  display: 'flex',
                  justifyContent: 'flex-end',
                  alignItems: 'center',
                  flexDirection: 'row',
                }}>
                <View style={{marginRight: 10}}>
                  <Button
                    color={colors.google}
                    title="Đóng"
                    onPress={() => refRBSheetCart.current.close()}
                  />
                </View>
                <Button
                  title="Đến giỏ hàng"
                  onPress={() => {
                    navigation.jumpTo('CartScreen');
                    refRBSheetCart.current.close();
                  }}
                />
              </View>
            </View>
          </RBSheet>
        </View>
      </ScrollView>
    </Bullets>
  );
}

const CustomStyle = {
  wrapper: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
  },
  draggableIcon: {
    backgroundColor: '#000',
  },
  borderColor: colors.black,
  borderWidth: 1,
};

const Styles = StyleSheet.create({
  detailContain: {
    backgroundColor: colors.white,
    padding: 15,
  },
  detailContainSecond: {
    marginTop: 10,
    height: 180,
  },
  name: {
    fontSize: 18,
  },
  groupPrice: {
    // textDecoration
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  price: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  oldPrice: {
    fontSize: 18,
    color: colors.gray,
    textDecorationLine: 'line-through',
    marginLeft: 15,
  },
  fz18: {
    fontSize: 18,
  },
  fwBold: {
    fontWeight: 'bold',
  },
  clBlue: {
    color: 'blue',
  },
  colStyle: {
    height: 150,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemCenter: {
    alignItems: 'center',
  },
  btnBuy: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.google,
    height: 50,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 5,
  },
  btnBuyDisable: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.gray,
    height: 50,
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 5,
  },
  btnBuyTitle: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: '#F194FF',
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },
  textInputStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#5c5c5c',
    borderBottomWidth: 0.5,
  },
  textInput: {
    color: '#999999',
    width: '100%',
    height: 50,
    marginLeft: 10,
    marginRight: -60,
    fontSize: 18,
  },
  detailTable: {
    justifyContent: 'flex-start',
    flexDirection: 'row',
  },
});

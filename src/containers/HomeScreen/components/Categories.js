import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ImageBackground,
  StyleSheet,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import colors from '../../../assets/styles/colors';
import Bullets from 'react-native-easy-content-loader';

export default function Categories({id, navigation}) {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let List = [];
    firestore()
      .collection('products')
      .limit(10)
      .where('categories', 'array-contains', id)
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(documentSnapshot => {
          const {productImage, name, price} = documentSnapshot.data();
          List.push({
            id: documentSnapshot.id,
            name: name,
            image: productImage,
            price: price,
          });
        });
      })
      .then(() => setData(List))
      .then(() => setLoading(false));
  }, [id]);

  return (
    <View>
      <Bullets
        loading={loading}
        active
        listSize={1}
        pHeight={[150]}
        pWidth={['100%']}>
        <FlatList
          numColumns={1}
          showsHorizontalScrollIndicator={false}
          data={data}
          keyExtractor={(item, index) => index + item}
          horizontal={true}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                navigation.navigate('HomeDetail', {
                  id: item.id,
                  name: item.name,
                  price: item.price,
                  image: item.image,
                })
              }
              style={[Styles.productList, Styles.featureProductList]}>
              <ImageBackground
                source={{uri: item.image}}
                style={Styles.lastProductListImage}
                imageStyle={Styles.imageStyle}
              />
              <Text style={Styles.featureItemName}>{item.name}</Text>
              <Text style={Styles.featureItemPrice}>{item.price} ₫</Text>
            </TouchableOpacity>
          )}
        />
      </Bullets>
    </View>
  );
}

const Styles = StyleSheet.create({
  productList: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 8,
  },
  lastProductList: {
    flexDirection: 'column',
    width: '46%',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },
  lastProductListImage: {
    height: 140,
    width: '100%',
    backgroundColor: colors.white,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  imageStyle: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  featureItemName: {
    textAlign: 'center',
    fontWeight: 'bold',
    margin: 3,
  },
  featureItemPrice: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: colors.secondary,
    marginBottom: 5,
  },
  featureProductList: {
    flexDirection: 'column',
    width: 172,
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },
});

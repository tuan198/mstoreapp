import * as React from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Animated,
  Easing,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import {useEffect, useState} from 'react';
import colors from '../../assets/styles/colors';
import {HomeStyles} from './home.style';
import Trademarks from './components/Trademarks';
import Products from './components/Products';
import Categories from './components/Categories';

export default function HomeScreen({navigation}) {
  const date = new Date().getDate();
  const month = new Date().getMonth() + 1;
  const day = new Date().getDay() + 1;
  const [animation] = useState(new Animated.Value(0));

  useEffect(() => {
    Animated.loop(
      Animated.timing(animation, {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      }),
    ).start();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const spin = animation.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  return (
    <ScrollView
      style={HomeStyles.container}
      showsVerticalScrollIndicator={false}>
      <View style={HomeStyles.header}>
        <Animated.Image
          style={[HomeStyles.logoHeader, {transform: [{rotate: spin}]}]}
          source={require('../../assets/images/logoRn.png')}
        />
        <Text style={HomeStyles.dayHeader}>
          {day === 2
            ? 'Thứ hai, '
            : day === 3
            ? 'Thứ ba, '
            : day === 4
            ? 'Thứ tư, '
            : day === 5
            ? 'Thứ năm, '
            : day === 6
            ? 'Thứ sáu, '
            : day === 7
            ? 'Thứ bảy, '
            : 'Chủ nhật, '}
          Ngày {date} Tháng {month}
        </Text>
      </View>
      <View style={HomeStyles.mt20}>
        <Trademarks navigation={navigation} />
      </View>
      <View style={HomeStyles.mt20}>
        {FlatListData.map(category => (
          <>
            <View style={HomeStyles.multiProductContent}>
              <Text style={HomeStyles.multiProductTitle}>{category.title}</Text>
              <TouchableOpacity
                style={HomeStyles.flexRow}
                onPress={() =>
                  navigation.navigate('ProductList', {
                    id: category.name,
                    name: category.title,
                    collection: 'categories',
                  })
                }>
                <Text>Xem thêm</Text>
                <Entypo
                  name={'chevron-small-right'}
                  size={20}
                  color={colors.black}
                />
              </TouchableOpacity>
            </View>
            {category.name !== 'Product' ? (
              <Categories navigation={navigation} id={category.name} />
            ) : (
              <Products navigation={navigation} />
            )}
          </>
        ))}
      </View>
    </ScrollView>
  );
}

const FlatListData = [
  {
    name: 'Business',
    title: 'Doanh nhân',
  },
  {
    name: 'Gaming',
    title: 'Gaming',
  },
  {
    name: 'Graphic',
    title: 'Đồ họa',
  },
  {
    name: 'Macbook',
    title: 'Macbook',
  },
  {
    name: 'Accessories',
    title: 'Phụ kiện',
  },
  {
    name: 'Product',
    title: 'Sản phẩm',
  },
];

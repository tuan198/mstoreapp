import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';

export const HomeStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  categoryItem: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 16,
    backgroundColor: 'white',
  },
  productList: {
    alignItems: 'center',
    textAlign: 'center',
    marginVertical: 8,
    marginHorizontal: 8,
  },
  productRecentList: {
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
  },
  listMultiProduct: {
    marginTop: 20,
    flexDirection: 'column',
  },
  multiProductContent: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  multiProductTitle: {
    fontWeight: 'bold',
    color: colors.eggBlue,
    fontSize: 20,
  },
  featureProductList: {
    flexDirection: 'column',
    width: 172,
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },
  lastProductList: {
    flexDirection: 'column',
    width: '46%',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'space-between',
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 10,
    backgroundColor: colors.alabaster,
  },

  header: {
    marginTop: 20,
    marginLeft: 20,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  logoHeader: {
    width: 56,
    height: 50,
    marginRight: 10,
  },
  dayHeader: {
    textTransform: 'uppercase',
    fontSize: 16,
    color: '#000000',
  },
  mt20: {
    marginTop: 20,
  },
  flexRow: {
    flexDirection: 'row',
  },
});

import * as React from 'react';
import {View, Text, TextInput, TouchableOpacity} from 'react-native';
import auth from '@react-native-firebase/auth';
import {useState} from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';
import {ForgotPasswordStyles, signinStyles} from './auth.styles';

export default function ForgotPassword({navigation}) {
  const [mess, setMess] = useState('');
  const [email, setEmail] = useState('');
  const [check, setCheck] = useState(false);

  const forgotPassword = Email => {
    auth()
      .sendPasswordResetEmail(Email)
      .then(function() {
        setMess('Check your email');
        setCheck(true);
      })
      .catch(function(e) {
        setMess(e.message);
        setCheck(false);
      });
  };

  return (
    <View style={ForgotPasswordStyles.container}>
      {check ? (
        <View style={ForgotPasswordStyles.linearGradient}>
          <View style={ForgotPasswordStyles.completeContent}>
            <Text style={ForgotPasswordStyles.completeTitle}>
              Kiểm tra email
            </Text>
            <Text style={ForgotPasswordStyles.completeValue}>
              Liên kết đặt lại mật khẩu đã được gửi tới email {email} của bạn.
              Kiểm tra email và đặt lại mật khẩu.
            </Text>
            <View style={ForgotPasswordStyles.check}>
              <Entypo color={'#2C8CFC'} size={100} name={'check'} />
            </View>
          </View>
        </View>
      ) : (
        <>
          <View style={ForgotPasswordStyles.containChecking}>
            <Text>{mess}</Text>
            <Text style={ForgotPasswordStyles.titleCheck}>
              Vui lòng nhập địa chỉ email để đặt lại mật khẩu của bạn.
            </Text>
            <TextInput
              value={email}
              onChangeText={mail => setEmail(mail)}
              placeholder={'Địa chỉ Email'}
              style={ForgotPasswordStyles.input}
            />
            <View style={ForgotPasswordStyles.loginNavigateContainer}>
              <Text style={signinStyles.registerTitle}>Đã có tài khoản? </Text>
              <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
                <Text style={signinStyles.registerBtn}>Đăng nhập</Text>
              </TouchableOpacity>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => forgotPassword(email)}
            style={ForgotPasswordStyles.btnForgot}>
            <Text style={ForgotPasswordStyles.btnTitle}>Đặt lại mật khẩu</Text>
          </TouchableOpacity>
        </>
      )}
    </View>
  );
}

import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';

export const signinStyles = StyleSheet.create({
  // Login --------------------------------------
  backgroundTrans: {
    opacity: 0.1,
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  logoStyle: {
    width: 250,
    height: 90,
    marginBottom: 20,
    // marginTop: 100,
  },
  containerLogin: {
    width: '100%',
    marginBottom: 30,
  },
  paper: {
    marginLeft: 30,
    marginRight: 30,
  },
  mb15: {
    marginBottom: 15,
  },
  mt15: {
    marginTop: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mb10: {
    marginBottom: 10,
  },
  mt10: {
    marginTop: 10,
  },
  ml5: {
    marginLeft: 5,
  },
  characterOr: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  characterTitle: {
    fontSize: 16,
    color: '#5c5c5c',
  },
  forgetPass: {
    marginTop: 10,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  forgetPassTitle: {
    color: colors.eggBlue,
    fontSize: 16,
  },
  errStyle: {
    color: colors.google,
  },
  inputContainer: {
    width: '100%',
    marginBottom: 20,
  },
  textInput: {
    color: '#999999',
    width: '100%',
    height: 50,
    marginLeft: 10,
    marginRight: -60,
    fontSize: 18,
  },
  configInput: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInputStyle: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: '#5c5c5c',
    borderBottomWidth: 0.5,
  },
  justifySB: {
    justifyContent: 'space-between',
  },
  buttonTitle: {
    color: 'white',
    fontSize: 16,
    marginLeft: 10,
  },
  button: {
    height: 50,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  btnLogin: {
    backgroundColor: '#00c8be',
  },
  btnSocialNetwork: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    marginBottom: 20,
  },
  btnFb: {
    backgroundColor: colors.facebook,
    // marginLeft: 10,
    marginRight: 5,
    width: 150,
    height: 50,
    borderRadius: 10,
  },
  btnGoogle: {
    backgroundColor: colors.google,
    marginLeft: 5,
    // marginRight: 10,
    height: 50,
    width: 150,
    borderRadius: 10,
  },
  btnIcon: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  registerContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 15,
  },
  registerTitle: {
    fontSize: 18,
    color: '#5c5c5c',
  },
  registerBtn: {
    color: '#00c8be',
    fontWeight: 'bold',
    fontSize: 18,
  },
});

export const registerStyles = StyleSheet.create({
  // Register --------------------------------------
  containerReg: {
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 10,
  },
  pickerStyle: {
    color: '#999999',
    marginLeft: -8,
    marginRight: -10,
  },
  containerCheckboxStyle: {
    backgroundColor: null,
    borderWidth: null,
    marginLeft: -10,
  },
  containerCheckboxTitle: {
    color: '#999999',
    fontWeight: null,
  },
  textInputReg: {
    borderBottomWidth: 1,
    borderBottomColor: '#5c5c5c',
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: -5,
    fontSize: 16,
  },
  errStyle: {
    color: colors.google,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#5c5c5c',
    marginTop: 20,
  },
  loginButton: {
    backgroundColor: '#00c8be',
    width: '100%',
    marginTop: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
  },
  loginButtonDisable: {
    backgroundColor: '#cccccc',
    width: '100%',
    marginTop: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
  },
  loginButtonTitle: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
  loginButtonTitleDisable: {
    color: '#666666',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export const ForgotPasswordStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'space-between',
  },
  linearGradient: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  completeContent: {
    display: 'flex',
    alignItems: 'center',
  },
  completeTitle: {
    color: colors.white,
    fontSize: 24,
    marginBottom: 10,
  },
  completeValue: {
    color: colors.white,
    fontSize: 16,
    textAlign: 'center',
    marginBottom: 50,
  },
  check: {
    height: 150,
    width: 150,
    borderRadius: 100,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 10,
  },
  containChecking: {
    marginTop: 30,
    margin: 20,
  },
  titleCheck: {
    color: colors.eggBlue,
    fontSize: 18,
    marginBottom: 20,
  },
  input: {
    borderBottomWidth: 0.5,
    borderColor: '#999999',
  },
  btnForgot: {
    backgroundColor: colors.eggBlue,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
    margin: 10,
    marginBottom: 50,
  },
  btnTitle: {
    color: colors.white,
    fontSize: 18,
    fontWeight: 'bold',
  },
  loginNavigateContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'row',
    marginTop: 15,
  },
});

import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Picker,
  ScrollView,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {useDispatch, useSelector} from 'react-redux';
import authActions from '../../redux/auth/actions';

import ModalLoading from '../../components/ModalLoading';
import {registerStyles, signinStyles} from './auth.styles';

export default function SignUp({navigation}) {
  const dispatch = useDispatch();
  const {loading} = useSelector(state => state.Auth);
  const [state, setState] = useState({
    gender: 'Male',
    checked: false,
  });
  const [user, setUser] = useState({
    email: '',
    password: '',
    rePassword: '',
    displayName: '',
    phoneNumber: '',
    gender: state.gender,
    photoURL:
      'https://helpiewp.com/wp-content/uploads/2017/12/user-roles-wordpress.png',
    errorMessage: null,
  });

  const handleSignUp = useCallback(() => {
    const {
      email,
      password,
      rePassword,
      phoneNumber,
      errorMessage,
      displayName,
      gender,
      photoURL,
    } = user;
    if (!email || !password || !displayName || !phoneNumber || !rePassword) {
      return [
        setUser(prevState => ({
          ...prevState,
          errorMessage: 'Name, phone, email, password or re-password not null',
        })),
      ];
    }
    if (password.length < 8) {
      return [
        setUser(prevState => ({
          ...prevState,
          errorMessage: 'Password must be greater than 8 characters',
        })),
      ];
    }
    if (password !== user.rePassword && user.rePassword.length > 0) {
      return [
        setUser(prevState => ({
          ...prevState,
          errorMessage: 'The 2 passwords do not match',
        })),
      ];
    }
    new Promise((resolve, reject) => {
      dispatch(
        authActions.register({
          email,
          password,
          data: {
            rePassword,
            phoneNumber,
            errorMessage,
            displayName,
            gender,
            photoURL,
          },
          resolve,
          reject,
        }),
      );
    })
      .then(() => {
        console.log('success');
      })
      .catch(e => {
        console.log(e.message);
      });
  }, [dispatch, user]);

  // const handleSignUp = () => {
  //   setLoading(true);
  //   const {
  //     email,
  //     password,
  //     displayName,
  //     photoURL,
  //     phoneNumber,
  //     rePassword,
  //   } = user;
  //   if (!email || !password || !displayName || !phoneNumber || !rePassword) {
  //     return [
  //       setUser(prevState => ({
  //         ...prevState,
  //         errorMessage: 'Name, phone, email, password or re-password not null',
  //       })),
  //       setLoading(false),
  //     ];
  //   }
  //   if (password.length < 8) {
  //     return [
  //       setUser(prevState => ({
  //         ...prevState,
  //         errorMessage: 'Password must be greater than 8 characters',
  //       })),
  //       setLoading(false),
  //     ];
  //   }
  //   if (password !== user.rePassword && user.rePassword.length > 0) {
  //     return [
  //       setUser(prevState => ({
  //         ...prevState,
  //         errorMessage: 'The 2 passwords do not match',
  //       })),
  //       setLoading(false),
  //     ];
  //   }
  //   auth()
  //     .createUserWithEmailAndPassword(email, password)
  //     .then(userCredentials => {
  //       return userCredentials.user.updateProfile({
  //         displayName,
  //         photoURL,
  //         phoneNumber,
  //       });
  //     })
  //     .then(() => setLoading(false))
  //     .then(() => {
  //       let userId = auth().currentUser.uid;
  //       let getUser = auth().currentUser;
  //       firestore()
  //         .collection('users')
  //         .doc(userId)
  //         .add({
  //           displayName: getUser.displayName,
  //           email: getUser.email,
  //           emailVerified: getUser.emailVerified,
  //           isAnonymous: getUser.isAnonymous,
  //           isAdmin: false,
  //           metadata: {
  //             creationTime: new Date(getUser.metadata.creationTime).toString(),
  //             lastSignInTime: new Date(
  //               getUser.metadata.lastSignInTime,
  //             ).toString(),
  //           },
  //           phoneNumber: getUser.phoneNumber,
  //           photoURL: user.photoURL,
  //           password: password,
  //           providerData: [],
  //           providerId: getUser.providerId,
  //           uid: getUser.uid,
  //           gender: user.gender,
  //         });
  //     })
  //     .then(() => setState({checked: false}))
  //     .then(() => navigation.navigate('HomeScreen'))
  //     .catch(error => {
  //       if (error.code === 'auth/email-already-in-use') {
  //         console.log('That email address is already in use!');
  //       }
  //       if (error.code === 'auth/invalid-email') {
  //         console.log('That email address is invalid!');
  //       }
  //       setUser(prevState => ({
  //         ...prevState,
  //         errorMessage: error.message,
  //       }));
  //       setLoading(false);
  //     });
  // };

  return (
    <ScrollView style={registerStyles.containerReg}>
      <Text style={registerStyles.title}>Thông tin cá nhân</Text>
      <View>
        {user.errorMessage && (
          <Text style={registerStyles.errStyle}>{user.errorMessage}</Text>
        )}
      </View>
      <TextInput
        style={registerStyles.textInputReg}
        autoCapitalize="none"
        onChangeText={displayName =>
          setUser(prevState => ({
            ...prevState,
            displayName,
          }))
        }
        value={user.displayName}
        placeholder={'Họ & Tên'}
      />
      <View style={registerStyles.textInputReg}>
        <Picker
          selectedValue={state.gender}
          style={registerStyles.pickerStyle}
          onValueChange={itemValue => setState({gender: itemValue})}>
          <Picker.Item label="Nam" value="Male" />
          <Picker.Item label="Nữ" value="Female" />
          <Picker.Item label="Tùy chỉnh" value="Custom" />
        </Picker>
      </View>
      <TextInput
        style={registerStyles.textInputReg}
        autoCapitalize="none"
        autoCompleteType={'tel'}
        dataDetectorTypes={'phoneNumber'}
        keyboardType={'numeric'}
        maxLength={10}
        textContentType={'telephoneNumber'}
        onChangeText={phoneNumber =>
          setUser(prevState => ({
            ...prevState,
            phoneNumber,
          }))
        }
        value={user.phoneNumber}
        placeholder={'Điện thoại'}
      />
      <Text style={registerStyles.title}>Thông tin tài khoản</Text>
      <TextInput
        style={registerStyles.textInputReg}
        autoCapitalize="none"
        onChangeText={email =>
          setUser(prevState => ({
            ...prevState,
            email,
          }))
        }
        value={user.email}
        placeholder={'Email'}
      />
      <TextInput
        style={registerStyles.textInputReg}
        secureTextEntry
        autoCapitalize="none"
        onChangeText={password =>
          setUser(prevState => ({
            ...prevState,
            password,
          }))
        }
        value={user.password}
        placeholder={'Mật khẩu'}
      />
      <TextInput
        style={registerStyles.textInputReg}
        secureTextEntry
        autoCapitalize="none"
        onChangeText={rePassword =>
          setUser(prevState => ({
            ...prevState,
            rePassword,
          }))
        }
        value={user.rePassword}
        placeholder={'Nhập lại mật khẩu'}
      />
      <CheckBox
        containerStyle={registerStyles.containerCheckboxStyle}
        title={
          <Text style={registerStyles.containerCheckboxTitle}>
            Đồng ý với điều khoản của ứng dụng
          </Text>
        }
        checked={state.checked}
        onPress={() => setState({checked: !state.checked})}
      />
      <View style={signinStyles.registerContainer}>
        <Text style={signinStyles.registerTitle}>Đã có tài khoản? </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
          <Text style={signinStyles.registerBtn}>Đăng nhập</Text>
        </TouchableOpacity>
      </View>
      <ModalLoading loading={loading} />
      <TouchableOpacity
        style={
          state.checked
            ? registerStyles.loginButton
            : registerStyles.loginButtonDisable
        }
        disabled={!state.checked}
        onPress={handleSignUp}>
        <Text
          style={
            state.checked
              ? registerStyles.loginButtonTitle
              : registerStyles.loginButtonTitleDisable
          }>
          Tạo tài khoản
        </Text>
      </TouchableOpacity>
    </ScrollView>
  );
}

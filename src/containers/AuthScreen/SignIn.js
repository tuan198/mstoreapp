import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  ImageBackground,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {Button} from 'react-native-elements';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import {signinStyles} from './auth.styles';
import authActions from '../../redux/auth/actions';
import {useDispatch, useSelector} from 'react-redux';
import ModalLoading from '../../components/ModalLoading';

export default function SignIn({navigation}) {
  const dispatch = useDispatch();
  const {isLoggedIn, loading} = useSelector(state => state.Auth);
  const [user, setUser] = useState({});
  const [showPass, setShowPass] = useState(false);
  const [btnMap] = useState(['fb', 'go']);

  useEffect(() => {
    dispatch(authActions.checkAuthorization());
  }, [dispatch]);

  const emailLogin = useCallback(() => {
    const {email, password} = user;
    new Promise((resolve, reject) => {
      dispatch(authActions.login({email, password, resolve, reject}));
    })
      .then(() => {
        console.log('logged in');
        navigation.navigate('HomeScreen');
      })
      .catch(e => {
        console.log(e.message);
      });
  }, [dispatch, navigation, user]);

  const facebookLogin = () => {};

  const googleLogin = () => {};

  return (
    <ImageBackground
      source={require('../../assets/images/13969656.jpg')}
      imageStyle={signinStyles.backgroundTrans}
      style={signinStyles.imgBackground}>
      <Text />
      <Image
        source={require('../../assets/images/logo.png')}
        style={signinStyles.logoStyle}
      />
      <View style={signinStyles.containerLogin}>
        <View style={signinStyles.paper}>
          <View style={signinStyles.inputContainer}>
            <View>
              {user.errorMessage && (
                <Text style={signinStyles.errStyle}>{user.errorMessage}</Text>
              )}
            </View>
            <View style={signinStyles.textInputStyle}>
              <Entypo name={'mail'} color={'#5c5c5c'} size={25} />
              <TextInput
                style={signinStyles.textInput}
                placeholderTextColor={'#999999'}
                autoCapitalize="none"
                onChangeText={email =>
                  setUser(prevState => ({
                    ...prevState,
                    email,
                  }))
                }
                value={user.email}
                placeholder={'Tên đăng nhập hoặc email'}
              />
            </View>
            <View style={[signinStyles.textInputStyle, signinStyles.justifySB]}>
              <View style={signinStyles.configInput}>
                <Entypo name={'lock'} color={'#5c5c5c'} size={25} />
                <TextInput
                  style={signinStyles.textInput}
                  placeholderTextColor={'#999999'}
                  secureTextEntry={!showPass}
                  autoCapitalize="none"
                  onChangeText={password =>
                    setUser(prevState => ({
                      ...prevState,
                      password,
                    }))
                  }
                  value={user.password}
                  dataDetectorTypes={'password'}
                  placeholder={'Mật khẩu'}
                />
              </View>
              <TouchableOpacity onPress={() => setShowPass(!showPass)}>
                <Entypo
                  name={showPass ? 'eye-with-line' : 'eye'}
                  color={'#5c5c5c'}
                  size={20}
                />
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate('ForgotPassword')}
              style={signinStyles.forgetPass}>
              <Text style={signinStyles.forgetPassTitle}>Quyên mật khẩu?</Text>
            </TouchableOpacity>
          </View>
          <View style={signinStyles.mb15}>
            <TouchableOpacity
              onPress={emailLogin}
              style={[signinStyles.button, signinStyles.btnLogin]}>
              <Text style={signinStyles.buttonTitle}>Đăng nhập</Text>
            </TouchableOpacity>
          </View>
          <View style={signinStyles.btnSocialNetwork}>
            {btnMap.map(title => (
              <Button
                onPress={() =>
                  title === 'fb' ? facebookLogin() : googleLogin()
                }
                buttonStyle={
                  title === 'fb' ? signinStyles.btnFb : signinStyles.btnGoogle
                }
                icon={() => (
                  <View style={signinStyles.btnIcon}>
                    <MaterialCommunityIcons
                      name={title === 'fb' ? 'facebook' : 'google'}
                      size={30}
                      color={'white'}
                    />
                    <Text style={signinStyles.buttonTitle}>
                      {title === 'fb' ? 'Facebook' : 'Google'}
                    </Text>
                  </View>
                )}
              />
            ))}
          </View>
          <View style={signinStyles.registerContainer}>
            <Text style={signinStyles.registerTitle}>Chưa có tài khoản? </Text>
            <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
              <Text style={signinStyles.registerBtn}>Đăng ký</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <ModalLoading loading={loading} />
    </ImageBackground>
  );
}

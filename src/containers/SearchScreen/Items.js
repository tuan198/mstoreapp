import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default function Items(props) {
  const name = props.name;
  const image = props.image;
  const price = props.price;
  // const id = props.id;
  // const play = props.urlPlay;

  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <Image source={{uri: image}} style={styles.img} />
      </View>
      <View style={styles.right}>
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>
          {name}
        </Text>
        <View>
          <Text style={styles.price}>{price} ₫</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
  },
  left: {
    height: 50,
    width: '15%',
  },
  right: {
    height: 50,
    width: '75%',
    flexDirection: 'column',
    paddingLeft: 10,
  },
  close: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
  },
  img: {
    height: 50,
    width: 50,
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
    color: 'black',
  },
  singer: {
    fontSize: 17,
    color: 'black',
  },
});

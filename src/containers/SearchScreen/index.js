import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ActivityIndicator,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {useSelector} from 'react-redux';
import {isEmpty} from 'lodash';
import firestore from '@react-native-firebase/firestore';
import Icon from 'react-native-vector-icons/FontAwesome';
import Items from './Items';

export default function SearchScreen({navigation}) {
  const [text, setText] = useState([]);
  const [textSearch, setTextSearch] = useState('');
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const {products} = useSelector(state => state.Product);
  const ref = firestore().collection('products');

  useEffect(() => {
    (async () => {
      const querySnapshot = await ref.limit(9).get();
      const list = [];
      querySnapshot.forEach(doc => {
        list.push({
          id: doc.id,
          name: doc.data().text,
          ...doc.data(),
        });
      });
      setText(list);
      setIsLoading(true);
    })();
  }, [ref]);

  // eslint-disable-next-line no-shadow
  const searchFilterFunction = text => {
    setTextSearch(text);
    setItems(
      products.filter(item => {
        console.log(item);
        return item.name.toLowerCase().includes(text.toLowerCase());
      }),
    );
  };

  const printSong = () => {
    if (textSearch === '') {
      return (
        <SafeAreaView>
          <ScrollView>
            <View>
              <View style={styles.container2}>
                <Text style={styles.header}>LỊCH SỬ</Text>
              </View>
              <View style={styles.listRecords}>
                {text.map((item, index) => (
                  <TouchableOpacity
                    onPress={() =>
                      navigation.navigate('HomeDetail', {id: item.id})
                    }>
                    <Items
                      key={index}
                      name={item.name}
                      price={item.price}
                      image={item.productImage}
                      onAddRecent={addRecentTOState}
                    />
                  </TouchableOpacity>
                ))}
              </View>
              {text.length > 9 ? (
                <View />
              ) : (
                <View style={styles.textViewMoreContainer}>
                  <TouchableOpacity onPress={loadMoreData}>
                    <Text style={styles.textViewMore}>Xem Thêm</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          </ScrollView>
        </SafeAreaView>
      );
    } else {
      return (
        <View style={styles.f1}>
          <View>
            {!isEmpty(items) ? (
              <>
                <Text style={styles.textTitle}>Sản phẩm</Text>
                <FlatList
                  data={items}
                  renderItem={({item, index}) => (
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate('HomeDetail', {id: item.id})
                      }>
                      <Items
                        key={index}
                        name={item.name}
                        price={item.price}
                        image={item.image}
                      />
                    </TouchableOpacity>
                  )}
                  keyExtractor={item => item.name}
                />
              </>
            ) : (
              <View style={styles.messageContainer}>
                {/*<Text style={styles.messageText}>*/}
                {/*  Không có kết quả tìm kiếm ?*/}
                {/*</Text>*/}
              </View>
            )}
          </View>
        </View>
      );
    }
  };

  const loadMoreData = () => {
    ref.onSnapshot(snapshot => {
      const texts = [];
      snapshot.forEach(doc => {
        texts.push({
          id: doc.id,
          name: doc.data().text,
        });
      });
      setText(texts);
      // console.log(this.state.text);
    });
  };

  const addRecentTOState = name => {
    searchFilterFunction(name);
    setTextSearch(name);
  };

  if (!isLoading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator color={'green'} size={'large'} />
      </View>
    );
  } else {
    return (
      <SafeAreaView style={styles.main}>
        <View style={styles.container}>
          <View style={styles.left}>
            <TouchableOpacity>
              <Icon name="search" size={25} color="#dcdcdc" />
            </TouchableOpacity>
            <TextInput
              placeholder="Tìm kiếm ..."
              style={styles.textInput}
              // eslint-disable-next-line no-shadow
              onChangeText={text => searchFilterFunction(text)}
              autoCapitalize={'none'}
              value={textSearch}
            />
          </View>
        </View>
        <View style={styles.f1}>{printSong()}</View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 15,
    padding: 10,
    // paddingHorizontal: 10,
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#000',
  },
  left: {
    height: 50,
    backgroundColor: '#ffffff',
    flex: 1,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 30,
    shadowColor: '#152734',
    shadowRadius: 20,
    shadowOpacity: 0.4,
  },
  right: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingLeft: 7,
  },
  main: {
    backgroundColor: 'white',
    height: '100%',
    flex: 1,
  },
  button: {
    backgroundColor: 'blue',
    height: '100%',
    width: 60,
    borderRadius: 30,
  },
  textInput: {
    marginLeft: 10,
    fontSize: 20,
    width: 250,
  },
  header: {
    fontSize: 25,
  },
  container2: {
    marginHorizontal: 15,
    // marginBottom: 15,
  },
  flatList: {
    flexWrap: 'wrap',
  },
  content: {
    alignItems: 'flex-start',
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  item: {
    padding: 10,
  },
  text: {
    fontSize: 15,
    color: 'black',
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  loadMoreBtn: {
    padding: 10,
    backgroundColor: '#800000',
    borderRadius: 4,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    color: 'white',
    fontSize: 15,
    textAlign: 'center',
  },
  findSinger: {
    height: 100,
    width: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  listRecords: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  textViewMoreContainer: {
    alignItems: 'center',
    paddingHorizontal: 10,
  },
  textViewMore: {
    fontSize: 17,
    color: '#C2257F',
  },
  f1: {
    flex: 1,
  },
  ml5: {
    marginLeft: 5,
  },
  textSinger: {
    fontSize: 25,
    color: 'black',
  },
  singer_image: {
    height: 70,
    width: 70,
    borderRadius: 35,
  },
  nameSinger: {
    color: '#059cff',
  },
  textTitle: {
    marginLeft: 10,
    fontSize: 25,
    color: 'black',
  },
  messageContainer: {
    marginTop: 300,
    alignItems: 'center',
  },
  messageText: {
    color: 'red',
    fontSize: 25,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';

export const SearchStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  searchContainer: {
    backgroundColor: 'white',
    borderBottomColor: 'white',
    borderTopColor: 'white',
  },
  searchInputContainer: {
    backgroundColor: colors.alabaster,
    borderColor: colors.gray,
    borderWidth: 0.5,
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.5,
    borderStyle: 'solid',
  },
});

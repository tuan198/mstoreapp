import * as React from 'react';
import {View, TouchableOpacity, ScrollView, Image} from 'react-native';
import {Content, Card, CardItem, Text, Toast} from 'native-base';
import colors from '../../assets/styles/colors';
import Feather from 'react-native-vector-icons/Feather';
import {CartStyles} from './cart.styles';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {useEffect, useState} from 'react';
// import {useDispatch, useSelector} from 'react-redux';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
// import cartActions from '../../redux/cart/actions';
import Bullets from 'react-native-easy-content-loader';
import {isEmpty} from 'lodash';

export default function CartScreen({navigation}) {
  // const {carts} = useSelector(state => state.Cart);
  // const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [Gia, SetGia] = useState([]);
  const [loading, setLoading] = useState(true);
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const [cartSize, setcartSize] = useState(0);
  const [cartID, setCartID] = useState(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps,no-shadow
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  useEffect(() => {
    if (user && !isEmpty(data)) {
      const uid = auth().currentUser.uid;
      var cartId = null;
      firestore()
        .collection('shopping-card')
        .doc(uid)
        .get()
        .then(doc => {
          return (cartId = doc.id);
        })
        .then(() => {
          setCartID(cartId);
        });
    }
  }, [data, user]);

  useEffect(() => {
    if (user) {
      // const uid = auth().currentUser.uid;
      // dispatch(cartActions.getCart({userId: uid}));
      const List = [];
      const S = [];
      const uid = auth().currentUser.uid;
      firestore()
        .collection('shopping-card')
        .doc(uid)
        .collection('products')
        .get()
        .then(querySnapshot => {
          const sizes = querySnapshot.size;
          setcartSize(sizes);
          querySnapshot.forEach(documentSnapshot => {
            const cart = documentSnapshot.data();
            List.push({...cart, id: documentSnapshot.id});
            S.push(Number(cart.price.split('.').join('')) * cart.quantum);
          });
        })
        .then(() => setData(List))
        .then(() => setLoading(false))
        .then(() => SetGia(S.reduce((a, b) => a + b)));
    }
  });

  const handleRemove = id => {
    const uid = auth().currentUser.uid;
    firestore()
      .collection('shopping-card')
      .doc(uid)
      .collection('products')
      .doc(id)
      .delete()
      .then(() =>
        Toast.show({
          text: 'Xóa thành công',
          buttonText: 'OK',
          position: 'bottom',
          type: 'success',
        }),
      );
  };

  const handleChangeQuantum = (id, plus, quantumNew) => {
    const uid = auth().currentUser.uid;
    firestore()
      .collection('shopping-card')
      .doc(uid)
      .collection('products')
      .doc(id)
      .update({
        quantum: plus ? quantumNew + 1 : quantumNew - 1,
      });
  };

  if (initializing) {
    return null;
  }

  return (
    <>
      {!user ? (
        <View style={CartStyles.container}>
          <Text />
          <View style={CartStyles.cart}>
            <Feather name={'shopping-cart'} size={80} color={colors.black} />
            <Text style={CartStyles.cartTitleFirst}>
              Giỏ hàng của bạn trống
            </Text>
            <Text style={CartStyles.cartTitleSecond}>
              Thêm một sản phẩm vào giỏ hàng
            </Text>
          </View>
          <TouchableOpacity
            style={CartStyles.loginButton}
            onPress={() => navigation.jumpTo('HomeScreen')}>
            <Text style={CartStyles.loginButtonTitle}>Thêm ngay</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <Bullets
          loading={loading}
          active
          listSize={6}
          pHeight={[25]}
          pWidth={['100%']}>
          <View style={CartStyles.container}>
            <ScrollView style={{width: '100%', padding: 10}}>
              <View style={{width: '100%', marginBottom: 15}}>
                {data.map(cart => (
                  <Content>
                    <TouchableOpacity
                      onPress={() =>
                        navigation.navigate('HomeDetail', {
                          id: cart.id,
                        })
                      }>
                      <Card>
                        <CardItem>
                          <View
                            style={{
                              width: '30%',
                              display: 'flex',
                              justifyContent: 'flex-start',
                            }}>
                            <Image
                              style={{minWidth: 80, minHeight: 80}}
                              source={{uri: cart.productImage}}
                            />
                          </View>
                          <View
                            style={{
                              margin: 10,
                              marginRight: 65,
                              width: '70%',
                            }}>
                            <Text style={{fontSize: 14}}>{cart.name}</Text>
                            <Text style={{fontSize: 11}}>
                              Cung cấp bởi {cart.trademark}
                            </Text>
                            <Text
                              style={{
                                fontSize: 16,
                                color: colors.google,
                                fontWeight: 'bold',
                              }}>
                              {cart.price} ₫
                            </Text>
                            {cart.oldPrice ? (
                              <Text
                                style={{
                                  fontSize: 13,
                                  textDecorationLine: 'line-through',
                                  color: colors.gray,
                                }}>
                                {cart.oldPrice} ₫
                              </Text>
                            ) : null}
                            <View
                              style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                              }}>
                              <TouchableOpacity
                                disabled={cart.quantum === 1}
                                onPress={() =>
                                  handleChangeQuantum(
                                    cart.id,
                                    false,
                                    cart.quantum,
                                  )
                                }>
                                <MaterialCommunityIcons
                                  size={25}
                                  color={
                                    cart.quantum === 1
                                      ? colors.grey
                                      : colors.black
                                  }
                                  name="minus-box-outline"
                                />
                              </TouchableOpacity>
                              <Text
                                style={{
                                  borderWidth: 0.5,
                                  borderColor: 'black',
                                  minWidth: 30,
                                  height: 20,
                                }}>
                                {cart.quantum}
                              </Text>
                              <TouchableOpacity
                                disabled={cart.quantum === cart.amount}
                                onPress={() =>
                                  handleChangeQuantum(
                                    cart.id,
                                    true,
                                    cart.quantum,
                                  )
                                }>
                                <MaterialCommunityIcons
                                  size={25}
                                  color={
                                    cart.quantum === cart.amount
                                      ? colors.grey
                                      : colors.black
                                  }
                                  name="plus-box-outline"
                                />
                              </TouchableOpacity>
                            </View>
                          </View>
                        </CardItem>
                        <CardItem style={{justifyContent: 'flex-end'}}>
                          <TouchableOpacity
                            style={{flexDirection: 'row', alignItems: 'center'}}
                            onPress={() => handleRemove(cart.id)}>
                            <MaterialCommunityIcons
                              size={25}
                              name="trash-can"
                              color={colors.google}
                            />
                            <Text style={{marginLeft: 5}}>
                              Xóa khỏi giỏ hàng
                            </Text>
                          </TouchableOpacity>
                        </CardItem>
                      </Card>
                    </TouchableOpacity>
                  </Content>
                ))}
              </View>
            </ScrollView>
            {cartSize !== 0 ? (
              <View style={CartStyles.btnBuyGroup}>
                <View style={CartStyles.moneyGroup}>
                  <Text>Thành tiền</Text>
                  <Text>
                    {Gia.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.') +
                      ' ₫'}
                  </Text>
                </View>
                <TouchableOpacity
                  style={CartStyles.btnBuy}
                  onPress={() =>
                    navigation.navigate('Checkout', {
                      id: cartID,
                      cartData: data,
                      money:
                        Gia.toString().replace(
                          /(\d)(?=(\d{3})+(?!\d))/g,
                          '$1.',
                        ) + ' ₫',
                    })
                  }>
                  <Text style={CartStyles.loginButtonTitle}>
                    Tiến hành đặt hàng
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={CartStyles.container}>
                <Text />
                <View style={CartStyles.cart}>
                  <Feather
                    name={'shopping-cart'}
                    size={80}
                    color={colors.black}
                  />
                  <Text style={CartStyles.cartTitleFirst}>
                    Giỏ hàng của bạn trống
                  </Text>
                  <Text style={CartStyles.cartTitleSecond}>
                    Thêm một sản phẩm vào giỏ hàng
                  </Text>
                </View>
                <TouchableOpacity
                  style={CartStyles.loginButton}
                  onPress={() => navigation.jumpTo('HomeScreen')}>
                  <Text style={CartStyles.loginButtonTitle}>Thêm ngay</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </Bullets>
      )}
    </>
  );
}

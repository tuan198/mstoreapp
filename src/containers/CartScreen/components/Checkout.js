import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Alert,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import colors from '../../../assets/styles/colors';
import {Card, CardItem} from 'native-base';
import Bullets from 'react-native-easy-content-loader';
import {CartStyles} from '../cart.styles';
import moment from 'moment';
import ModalLoading from '../../../components/ModalLoading';

export default function Checkout({navigation, route}) {
  const {id, cartData, money} = route.params;
  const [address, setAddress] = useState({});
  const [loading, setLoading] = useState(true);
  const [mdLoading, setMdLoading] = useState(false);
  const date = Date.now();

  useEffect(() => {
    const userId = auth().currentUser.uid;
    firestore()
      .collection('users')
      .doc(userId)
      .get()
      .then(doc => {
        const dataUser = doc.data();
        setAddress({...dataUser});
      })
      .then(() => setLoading(false));
  });

  const handleSubmit = async () => {
    setMdLoading(true);
    await firestore()
      .collection('invoices')
      .doc()
      .set({
        ...address.address,
        createDate: moment(date).format('DD/MM/YYYY HH:mm:ss'),
        status: 'Chờ xử lý',
        uid: id,
        products: [...cartData],
        totalMoney: money,
      });
    await deleteCollection(firestore(), 'products', 10);
    setMdLoading(false);
  };

  function deleteCollection(db, collectionPath, batchSize) {
    let collectionRef = db
      .collection('shopping-card')
      .doc(id)
      .collection('products');
    let query = collectionRef.orderBy('__name__').limit(batchSize);
    return new Promise((resolve, reject) => {
      deleteQueryBatch(db, query, resolve, reject);
    });
  }

  function deleteQueryBatch(db, query, resolve, reject) {
    query
      .get()
      .then(snapshot => {
        // When there are no documents left, we are done
        if (snapshot.size === 0) {
          return 0;
        }
        // Delete documents in a batch
        let batch = db.batch();
        snapshot.docs.forEach(doc => {
          batch.delete(doc.ref);
        });

        return batch.commit().then(() => {
          setMdLoading(false);
          resolve();
          return snapshot.size;
        });
      })
      // .then(numDeleted => {
      //   if (numDeleted === 0) {
      //     resolve();
      //     return;
      //   }
      //   // Recurse on the next process tick, to avoid
      //   // exploding the stack.
      //   process.nextTick(() => {
      //     deleteQueryBatch(db, query, resolve, reject);
      //   });
      //   setMdLoading(false);
      //   resolve();
      // })
      .catch(reject);
  }

  return (
    <ScrollView>
      <ModalLoading loading={mdLoading} />
      <Bullets
        loading={loading}
        active
        listSize={6}
        pHeight={[25]}
        pWidth={['100%']}>
        <View>
          <View style={{backgroundColor: 'white', marginTop: 10, padding: 20}}>
            <Text
              style={{
                color: colors.eggBlue,
                fontSize: 20,
                fontWeight: 'bold',
                marginBottom: 20,
              }}>
              Thông tin địa chỉ
            </Text>
            {address.address ? (
              <>
                <Text style={{fontSize: 16}}>
                  Họ tên: {address.address.name}
                </Text>
                <Text style={{fontSize: 16}}>
                  Email: {address.address.email}
                </Text>
                <Text style={{fontSize: 16}}>
                  Số điện thoại: {address.address.phoneNumber}
                </Text>
                <Text style={{fontSize: 16}}>
                  Địa chỉ giao hàng: {address.address.address}
                </Text>
              </>
            ) : (
              <>
                <TouchableOpacity
                  style={{paddingTop: 20, paddingBottom: 10}}
                  onPress={() => navigation.navigate('Address')}>
                  <Text style={{fontSize: 16}}>
                    Địa chỉ của bạn trống. Click để thêm địa chỉ
                  </Text>
                </TouchableOpacity>
              </>
            )}
          </View>
          <View style={{backgroundColor: 'white', marginTop: 10, padding: 20}}>
            <Text
              style={{
                color: colors.eggBlue,
                fontSize: 20,
                fontWeight: 'bold',
                marginBottom: 20,
              }}>
              Thông tin giỏ hàng
            </Text>
            {cartData.map(ct => (
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('HomeDetail', {
                    id: ct.id,
                  });
                }}>
                <Card>
                  <CardItem>
                    <View
                      style={{
                        width: '30%',
                        display: 'flex',
                        justifyContent: 'flex-start',
                      }}>
                      <Image
                        style={{minWidth: 80, minHeight: 80}}
                        source={{uri: ct.productImage}}
                      />
                    </View>
                    <View
                      style={{
                        margin: 10,
                        marginRight: 65,
                        width: '70%',
                      }}>
                      <Text style={{fontSize: 14}}>{ct.name}</Text>
                      <Text
                        style={{
                          fontSize: 16,
                          color: colors.google,
                          fontWeight: 'bold',
                        }}>
                        {ct.price} ₫
                      </Text>
                      <Text>Số lượng: {ct.quantum}</Text>
                    </View>
                  </CardItem>
                </Card>
              </TouchableOpacity>
            ))}
          </View>
        </View>
        <View style={CartStyles.btnBuyGroup}>
          <View style={CartStyles.moneyGroup}>
            <Text>Thành tiền</Text>
            <Text>{money}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
              marginTop: 10,
              marginBottom: 20,
            }}>
            <TouchableOpacity
              style={[CartStyles.btnCheckout, {backgroundColor: colors.blue}]}
              onPress={() => navigation.navigate('CartScreen')}>
              <Text style={CartStyles.loginButtonTitle}>Hủy</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[CartStyles.btnCheckout, {backgroundColor: colors.google}]}
              onPress={() =>
                handleSubmit().then(() =>
                  Alert.alert(
                    'Đặt hàng',
                    'Đã thêm đơn hàng thành công',
                    [
                      {
                        text: 'OK',
                        onPress: () => navigation.navigate('CartScreen'),
                      },
                    ],
                    {cancelable: false},
                  ),
                )
              }>
              <Text style={CartStyles.loginButtonTitle}>
                Tiến hành đặt hàng
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Bullets>
    </ScrollView>
  );
}

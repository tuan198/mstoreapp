import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';

export const CartStyles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cart: {
    alignItems: 'center',
  },
  cartTitleFirst: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 20,
  },
  cartTitleSecond: {
    fontSize: 16,
    marginTop: 20,
  },
  loginButton: {
    backgroundColor: colors.eggBlue,
    width: 200,
    marginBottom: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 25,
  },
  loginButtonTitle: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
  moneyGroup: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  btnBuyGroup: {
    backgroundColor: colors.white,
    width: '100%',
    alignItems: 'center',
    padding: 20,
  },
  btnBuy: {
    backgroundColor: colors.google,
    width: '100%',
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
    height: 50,
  },
  btnCheckout: {
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
    width: 180,
    height: 50,
    margin: 10,
  },
});

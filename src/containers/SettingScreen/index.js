import React, {useEffect, useState} from 'react';
import {Text, View, ScrollView, Alert, TouchableOpacity} from 'react-native';
import {ListItem, Avatar} from 'react-native-elements';
import AntDesign from 'react-native-vector-icons/AntDesign';
import colors from '../../assets/styles/colors';
import {SettingStyles} from './setting.styles';
import ModalAbout from '../../components/ModalAbout';
import auth from '@react-native-firebase/auth';

export default function SettingScreen({navigation}) {
  const [modal, setModal] = useState(false);
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const userDisplay = auth().currentUser;

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  if (initializing) {
    return null;
  }

  const handleSignOut = () => {
    auth()
      .signOut()
      .then(() => {
        console.log('Logout');
      });
  };

  return (
    <View style={SettingStyles.container}>
      {user ? (
        <TouchableOpacity
          onPress={() => navigation.navigate('Profile')}
          style={SettingStyles.banner}>
          <View style={SettingStyles.bannerProfile}>
            <Avatar
              rounded
              size={100}
              containerStyle={SettingStyles.avatarContainer}
              source={{
                uri: userDisplay.providerData[0].photoURL,
              }}
            />
            <View style={SettingStyles.ml20}>
              <Text style={SettingStyles.displayNameStyle}>
                {userDisplay.providerData[0].displayName}
              </Text>
              <Text style={SettingStyles.emailStyle}>
                {userDisplay.providerData[0].email}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={() => navigation.navigate('SignIn')}
          style={SettingStyles.banner}>
          <View style={SettingStyles.bannerProfile}>
            <Avatar
              rounded
              size={100}
              containerStyle={SettingStyles.avatarContainer}
              source={require('../../assets/images/logoRn.png')}
            />
            <View style={SettingStyles.ml20}>
              <Text style={SettingStyles.displayNameStyle}>Đăng nhập</Text>
              <Text style={SettingStyles.emailStyle}>Bạn chưa đăng nhập</Text>
            </View>
          </View>
        </TouchableOpacity>
      )}
      <ScrollView>
        {(user ? dataUser : data).map((item, i) => (
          <ListItem
            onPress={() =>
              item.navigate === 'About'
                ? setModal(!modal)
                : navigation.navigate(item.navigate)
            }
            key={i}
            title={item.title}
            leftIcon={() => (
              <AntDesign name={item.icon} color={colors.gray} size={25} />
            )}
            bottomDivider
            chevron
            titleStyle={SettingStyles.listViewTitle}
            containerStyle={SettingStyles.listViewContainer}
          />
        ))}
        {user && (
          <TouchableOpacity
            style={SettingStyles.btnLogout}
            activeOpacity={0.5}
            onPress={() =>
              Alert.alert(
                'Đăng xuất',
                'Bạn có muốn đăng xuất khỏi ứng dụng không?',
                [
                  {
                    text: 'Không',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                  },
                  {text: 'Có', onPress: handleSignOut},
                ],
                {cancelable: true},
              )
            }>
            <Text style={SettingStyles.btnLogoutTitle}>Đăng xuất</Text>
          </TouchableOpacity>
        )}
      </ScrollView>
      <ModalAbout modal={modal} setModal={setModal} />
    </View>
  );
}

const data = [
  {
    title: 'Cài đặt',
    icon: 'setting',
    navigate: 'SettingMenu',
  },
  {
    title: 'Thông tin về ứng dụng',
    icon: 'exclamationcircleo',
    navigate: 'About',
  },
  {
    title: 'Liên hệ',
    icon: 'phone',
    navigate: 'ContactUs',
  },
];

const dataUser = [
  {
    title: 'Quản lý đơn hàng',
    icon: 'profile',
    navigate: 'Orders',
  },
  {
    title: 'Sổ địa chỉ',
    icon: 'enviromento',
    navigate: 'Address',
  },
  {
    title: 'Cài đặt',
    icon: 'setting',
    navigate: 'SettingMenu',
  },
  {
    title: 'Thông tin về ứng dụng',
    icon: 'exclamationcircleo',
    navigate: 'About',
  },
  {
    title: 'Liên hệ',
    icon: 'phone',
    navigate: 'ContactUs',
  },
];

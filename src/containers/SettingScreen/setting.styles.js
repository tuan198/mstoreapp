import {StyleSheet} from 'react-native';
import colors from '../../assets/styles/colors';

export const SettingStyles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.alabaster,
  },
  banner: {
    backgroundColor: colors.white,
    marginBottom: 15,
  },
  bannerProfile: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    margin: 20,
    marginTop: 40,
    marginBottom: 40,
  },
  avatarContainer: {
    borderWidth: 1,
    borderColor: colors.eggBlue,
  },
  ml20: {
    marginLeft: 20,
  },
  displayNameStyle: {
    fontWeight: 'bold',
    fontSize: 24,
  },
  emailStyle: {
    fontSize: 16,
  },
  listViewTitle: {
    color: colors.gray,
  },
  listViewContainer: {
    height: 50,
  },
  btnLogout: {
    backgroundColor: colors.white,
    height: 50,
    margin: 20,
    borderColor: colors.eggBlue,
    borderWidth: 1,
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnLogoutTitle: {
    color: colors.eggBlue,
    fontSize: 20,
  },
});

import * as React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Picker,
  TouchableOpacity,
  Platform,
  Image,
} from 'react-native';
import colors from '../../../assets/styles/colors';
import {useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import AntDesign from 'react-native-vector-icons/AntDesign';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';
import auth from '@react-native-firebase/auth';

export default function Profile() {
  const userDisplay = auth().currentUser;
  console.log(userDisplay.providerData[0]);
  const [state, setState] = useState({
    gender: 'Male',
    checked: false,
  });
  const [imgUpload, setImgUpload] = useState({
    filePath: {},
  });
  const [date, setDate] = useState(new Date());
  const dateTime =
    new Date().getDate().toString() +
    new Date().getMonth().toString() +
    new Date().getFullYear().toString() +
    new Date().getHours().toString() +
    new Date().getMinutes().toString() +
    new Date().getSeconds().toString();
  console.log(dateTime);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);

  const ImgPicker = () => {
    const options = {
      title: 'Chọn ảnh hồ sơ',
      cancelButtonTitle: 'Thoát',
      takePhotoButtonTitle: 'Camera',
      chooseFromLibraryButtonTitle: 'Chọn từ thư viện',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    ImagePicker.showImagePicker(options, response => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        setImgUpload({
          filePath: source,
        });
      }
    });
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === 'ios');
    setDate(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  return (
    <View style={Styles.container}>
      <View style={Styles.page100}>
        <View style={Styles.page}>
          <Text style={Styles.title}>Thông tin tài khoản:</Text>
        </View>
        {/*{Upload ?  : null}*/}
        <View style={Styles.page}>
          <View style={Styles.content}>
            <Text>Ảnh hồ sơ:</Text>
            <TouchableOpacity
              onPress={() => ImgPicker()}
              disabled={
                userDisplay.providerData[0].providerId === 'facebook.com' ||
                userDisplay.providerData[0].providerId === 'google.com'
              }
              style={Styles.uploadButton}>
              <Image
                source={
                  _.isEmpty(imgUpload.filePath)
                    ? {uri: userDisplay.providerData[0].photoURL}
                    : imgUpload.filePath
                }
                style={Styles.imgStyle}
              />
              <View style={Styles.uploadTitle}>
                <AntDesign name={'totop'} color={'#999999'} size={20} />
                <Text style={Styles.uploadLabel}>Upload</Text>
              </View>
            </TouchableOpacity>
          </View>
          <Text>Họ tên:</Text>
          <TextInput
            style={Styles.textInput}
            editable={userDisplay.providerData[0].providerId === 'password'}
            selectTextOnFocus={
              userDisplay.providerData[0].providerId === 'password'
            }
            autoCapitalize="none"
            onChangeText={() => console.log('name')}
            // value={userDisplay.providerData[0].displayName}
            placeholder={userDisplay.providerData[0].displayName}
          />
          <Text>Email:</Text>
          <TextInput
            style={Styles.textInput}
            autoCapitalize="none"
            editable={false}
            selectTextOnFocus={false}
            onChangeText={() => console.log('email')}
            // value={userDisplay.providerData[0].email}
            placeholder={userDisplay.providerData[0].email}
          />
          <Text>Số điện thoại:</Text>
          <TextInput
            style={Styles.textInput}
            autoCapitalize="none"
            keyboardType={'numeric'}
            maxLength={10}
            onChangeText={() => console.log('phone')}
            // value={userDisplay.providerData[0].phoneNumber}
            placeholder={userDisplay.providerData[0].phoneNumber}
          />
          <Text>Ngày sinh:</Text>
          {show && (
            <DateTimePicker
              testID="dateTimePicker"
              timeZoneOffsetInMinutes={0}
              value={date}
              mode={mode}
              is24Hour={true}
              display="default"
              onChange={onChange}
            />
          )}
          <View style={Styles.datePicker}>
            <TextInput
              style={Styles.dateInput}
              autoCapitalize="none"
              keyboardType={'numeric'}
              // maxLength={4}
              editable={false}
              selectTextOnFocus={false}
              onChangeText={() => console.log('year')}
              placeholder={
                date.getDate().toString() +
                '/' +
                date.getMonth().toString() +
                '/' +
                date.getFullYear().toString()
              }
            />
            <TouchableOpacity
              onPress={showDatepicker}
              disabled={
                userDisplay.providerData[0].providerId === 'facebook.com' ||
                userDisplay.providerData[0].providerId === 'google.com'
              }>
              <AntDesign name={'calendar'} size={20} color={'#5c5c5c'} />
            </TouchableOpacity>
          </View>
          <Text>Giới tính:</Text>
          <View style={Styles.textInput}>
            <Picker
              selectedValue={state.gender}
              style={[
                Styles.pickerStyle,
                userDisplay.providerData[0].providerId === 'password'
                  ? Styles.colorBlack
                  : Styles.colorDisable,
              ]}
              enabled={userDisplay.providerData[0].providerId === 'password'}
              onValueChange={itemValue => setState({gender: itemValue})}>
              <Picker.Item label="Nam" value="Male" />
              <Picker.Item label="Nữ" value="Female" />
              <Picker.Item label="Tùy chỉnh" value="Custom" />
            </Picker>
          </View>
        </View>
      </View>
      <View style={Styles.page}>
        <TouchableOpacity style={Styles.btnSave}>
          <Text style={Styles.btnTitle}>Lưu thay đổi</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  page: {
    width: '90%',
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    borderBottomColor: '#999999',
    marginBottom: 20,
    paddingBottom: 10,
  },
  page100: {
    width: '100%',
    backgroundColor: colors.white,
    alignItems: 'center',
    paddingTop: 15,
    paddingBottom: 5,
  },
  btnSave: {
    marginTop: 20,
    marginBottom: 20,
    borderRadius: 2,
    display: 'flex',
    alignItems: 'center',
    backgroundColor: colors.google,
  },
  uploadButton: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: 5,
  },
  uploadTitle: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  uploadLabel: {
    fontSize: 10,
    color: '#999999',
  },
  btnTitle: {
    color: colors.white,
    fontSize: 20,
    marginTop: 15,
    marginBottom: 15,
  },
  datePicker: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    marginBottom: 20,
    paddingRight: 5,
    borderBottomColor: '#5c5c5c',
  },
  pickerStyle: {
    marginLeft: -8,
    marginRight: -10,
    marginBottom: -10,
  },
  imgStyle: {
    width: 50,
    height: 50,
    marginRight: 10,
  },
  colorBlack: {
    color: 'black',
  },
  colorDisable: {
    color: '#999999',
  },
  dateInput: {
    padding: 0,
    minWidth: 60,
    flex: 1,
  },
  title: {
    fontSize: 20,
    marginTop: 15,
    marginBottom: 20,
    color: colors.eggBlue,
  },
  character: {
    marginRight: 15,
    marginLeft: 15,
  },
  textInput: {
    borderBottomWidth: 0.5,
    borderBottomColor: '#5c5c5c',
    marginBottom: 20,
    paddingLeft: -5,
    fontSize: 16,
    padding: 0,
  },
});

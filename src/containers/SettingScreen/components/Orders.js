import React, {useEffect, useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import _ from 'lodash';
import Feather from 'react-native-vector-icons/Feather';
import colors from '../../../assets/styles/colors';

export default function Orders({navigation}) {
  const userId = auth().currentUser.uid;
  const [data, setData] = useState([]);

  useEffect(() => {
    const List = [];
    firestore()
      .collection('orders')
      .doc(userId)
      .collection('orders')
      .get()
      .then(querySnapshot => {
        querySnapshot.forEach(documentSnapshot => {
          const order = documentSnapshot.data();
          List.push({...order, id: documentSnapshot.id});
        });
      })
      .then(() => setData(List));
  }, [userId]);

  console.log(data);

  return (
    <>
      {_.isEmpty(data) ? (
        <View style={Styles.container}>
          <Text />
          <View style={Styles.cart}>
            <Feather name={'shopping-cart'} size={80} color={colors.black} />
            <Text style={Styles.cartTitleFirst}>Đơn hàng của bạn trống</Text>
            <Text style={Styles.cartTitleSecond}>Mua thêm sản phẩm</Text>
          </View>
          <TouchableOpacity
            style={Styles.loginButton}
            onPress={() => navigation.jumpTo('HomeScreen')}>
            <Text style={Styles.loginButtonTitle}>Thêm ngay</Text>
          </TouchableOpacity>
        </View>
      ) : (
        <View>
          <Text>Order</Text>
        </View>
      )}
    </>
  );
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  cart: {
    alignItems: 'center',
  },
  cartTitleFirst: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 20,
  },
  cartTitleSecond: {
    fontSize: 16,
    marginTop: 20,
  },
  loginButton: {
    backgroundColor: colors.eggBlue,
    width: 200,
    marginBottom: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 25,
  },
  loginButtonTitle: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
  moneyGroup: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15,
  },
  btnBuyGroup: {
    backgroundColor: colors.white,
    width: '100%',
    alignItems: 'center',
    padding: 20,
  },
  btnBuy: {
    backgroundColor: colors.google,
    width: '100%',
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
    height: 50,
  },
});

import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Linking,
  Platform,
  StyleSheet,
} from 'react-native';
import colors from '../../../assets/styles/colors';
import AntDesign from 'react-native-vector-icons/Entypo';

export default function ContactUs() {
  const [conatct] = useState([
    {
      title: '+(84) 389 869 911',
      icon: 'phone',
      color: '#03fc5a',
      id: 'phone',
    },
    {
      title: 'nguyentuan.xpt@gmail.com',
      icon: 'mail',
      color: '#B82739',
      id: 'mail',
    },
    {
      title: 'Hanoi, Vietnamese',
      icon: 'location-pin',
      color: '#E90026',
      id: 'local',
    },
    {
      title: 'fb.com/tuanxpt',
      icon: 'facebook-with-circle',
      color: '#4F53B5',
      id: 'fb',
    },
    {
      title: 'twitter.com/tuanxpt',
      icon: 'twitter',
      color: '#2D92F6',
      id: 'tw',
    },
  ]);

  const Link = value => {
    return Linking.openURL(
      value.id === 'phone'
        ? Platform.OS === 'android'
          ? `tel:${'0389869911'}`
          : `telprompt:${'0389869911'}`
        : value.id === 'mail'
        ? Platform.OS === 'android'
          ? 'mailto:nguyentuan.xpt@gmail.com?subject=[MS] - Contact us&body=Dear'
          : 'mailto:nguyentuan.xpt@gmail.com&subject=[MS] - Contact us&body=Dear'
        : value.id === 'local'
        ? Platform.OS === 'android'
          ? 'geo:0,0?q=21.0351937+105.7650522'
          : 'maps:0,0?q=21.0351937+105.7650522'
        : value.id === 'fb'
        ? 'https://www.facebook.com/n/?tuanxpt/'
        : value.id === 'tw'
        ? 'https://www.twitter.com/tuanxpt'
        : '',
    );
  };

  return (
    <View>
      <View style={Styles.container}>
        <Text style={Styles.title}>Liên hệ</Text>
      </View>

      {conatct.map(st => (
        <TouchableOpacity
          onPress={() => Link(st)}
          style={[Styles.bgWhite, st.id === 'phone' ? Styles.mt10 : null]}>
          <View style={[Styles.contact, st.id === 'tw' ? null : Styles.bbw]}>
            <View style={Styles.content}>
              <AntDesign name={st.icon} size={20} color={st.color} />
              <Text style={Styles.value}>{st.title}</Text>
            </View>
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 80,
    paddingLeft: 24,
  },
  bgWhite: {
    backgroundColor: colors.white,
  },
  mt10: {
    marginTop: 10,
  },
  contact: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: 50,
    marginRight: 20,
    marginLeft: 20,
    borderBottomColor: '#999999',
  },
  bbw: {
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.eggBlue,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  value: {
    fontSize: 16,
    color: '#5c5c5c',
    marginLeft: 20,
  },
});

import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import colors from '../../../../assets/styles/colors';
import {Jiro} from 'react-native-textinput-effects';
import auth from '@react-native-firebase/auth';
import ModalLoading from '../../../../components/ModalLoading';

export default function ChangePassword() {
  const [title] = useState(['oldPass', 'newPass', 'rePass']);
  const [loading, setLoading] = useState(false);
  const [mess, setMess] = useState('');
  const [state, setState] = useState({});
  const reauthenticate = currentPassword => {
    const user = auth().currentUser;
    const cred = auth.EmailAuthProvider.credential(user.email, currentPassword);
    return user.reauthenticateWithCredential(cred);
  };

  const onChangePasswordPress = () => {
    setLoading(true);
    if (state.rePassword !== state.newPassword) {
      return [
        setMess("Password and re-password don't match"),
        setLoading(false),
      ];
    }
    if (
      state.rePassword.length <= 7 ||
      state.newPassword.length < 7 ||
      state.currentPassword.length < 7
    ) {
      return [
        setMess('Password must not be less than 8 characters'),
        setLoading(false),
      ];
    }
    if (!state.rePassword || !state.newPassword || !state.currentPassword) {
      return [
        setMess('Current password, password and re-password not null'),
        setLoading(false),
      ];
    }
    reauthenticate(state.currentPassword)
      .then(() => {
        const user = auth().currentUser;
        user
          .updatePassword(state.newPassword)
          .then(() => {
            setMess('Password was changed');
            setState({});
            setLoading(false);
          })
          .catch(error => {
            setMess(error.message);
            setLoading(false);
          });
      })
      .catch(error => {
        setMess(error.message);
        setLoading(false);
      });
  };

  return (
    <View style={Styles.container}>
      <ModalLoading loading={loading} />
      <View>
        <View style={Styles.headerTitlePass}>
          <Text style={Styles.title}>Đổi mật khẩu</Text>
        </View>
        <View style={Styles.contain}>
          {mess === '' ? null : (
            <Text
              style={
                mess === 'Password was changed'
                  ? Styles.completeMess
                  : Styles.errMess
              }>
              {mess}
            </Text>
          )}
          {title.map(label => (
            <Jiro
              label={
                label === 'oldPass'
                  ? 'Mật khẩu cũ'
                  : label === 'newPass'
                  ? 'Mật khẩu mới'
                  : 'Nhập lại mật khẩu mới'
              }
              autoCapitalize={'none'}
              autoCorrect={false}
              secureTextEntry={true}
              borderColor={'#999999'}
              inputPadding={16}
              inputStyle={Styles.colorWhite}
              value={
                label === 'oldPass'
                  ? state.currentPassword
                  : label === 'newPass'
                  ? state.newPassword
                  : state.rePassword
              }
              onChangeText={text => {
                label === 'oldPass'
                  ? setState(prevState => ({
                      ...prevState,
                      currentPassword: text,
                    }))
                  : label === 'newPass'
                  ? setState(prevState => ({
                      ...prevState,
                      newPassword: text,
                    }))
                  : setState(prevState => ({
                      ...prevState,
                      rePassword: text,
                    }));
              }}
            />
          ))}
        </View>
      </View>
      <View>
        <TouchableOpacity
          onPress={onChangePasswordPress}
          style={Styles.btnComplete}>
          <Text style={Styles.btnTitle}>XÁC NHẬN</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  headerTitlePass: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 80,
    paddingLeft: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.eggBlue,
  },
  contain: {
    backgroundColor: 'white',
    marginTop: 10,
    padding: 10,
    paddingBottom: 30,
  },
  errMess: {
    color: colors.google,
    fontSize: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  completeMess: {
    color: colors.eggBlue,
    fontSize: 16,
    marginLeft: 10,
    marginRight: 10,
  },
  colorWhite: {
    color: colors.white,
  },
  btnComplete: {
    backgroundColor: colors.eggBlue,
    height: 50,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    margin: 20,
  },
  btnTitle: {
    color: colors.white,
    fontSize: 18,
  },
});

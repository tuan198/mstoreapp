import * as React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {useEffect, useState} from 'react';
import colors from '../../../../assets/styles/colors';
import AntDesign from 'react-native-vector-icons/AntDesign';
import auth from '@react-native-firebase/auth';

export default function SettingMenu({navigation}) {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  if (initializing) {
    return null;
  }

  return (
    <View>
      <View style={Styles.container}>
        <Text style={Styles.title}>Cài đặt</Text>
      </View>
      {(user ? settingListUser : settingList).map(setting => (
        <TouchableOpacity
          onPress={() => navigation.navigate(setting.navigate)}
          style={[
            Styles.bgWhite,
            user
              ? setting.navigate === 'ForgotPassword'
                ? null
                : Styles.mt10
              : Styles.mt10,
          ]}>
          <View
            style={[
              Styles.listSt,
              setting.navigate === 'ChangePassword' ? Styles.bbw : null,
            ]}>
            <Text style={Styles.switch}>{setting.title}</Text>
            <AntDesign name={'right'} size={20} color={'#999999'} />
          </View>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const settingList = [
  {
    title: 'Quyên mật khẩu',
    navigate: 'ForgotPassword',
  },
];

const settingListUser = [
  {
    title: 'Đổi mật khẩu',
    navigate: 'ChangePassword',
  },
  {
    title: 'Quyên mật khẩu',
    navigate: 'ForgotPassword',
  },
];

const Styles = StyleSheet.create({
  container: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 80,
    paddingLeft: 24,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.eggBlue,
  },
  bgWhite: {
    backgroundColor: colors.white,
  },
  mt10: {
    marginTop: 10,
  },
  listSt: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 50,
    marginRight: 20,
    marginLeft: 20,
    borderBottomColor: '#999999',
  },
  bbw: {
    borderBottomWidth: 0.5,
  },
  switch: {
    fontSize: 16,
    color: '#5c5c5c',
  },
});

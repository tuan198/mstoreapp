import * as React from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import {useEffect, useState} from 'react';
import auth from '@react-native-firebase/auth';
import {CartStyles} from '../../CartScreen/cart.styles';
import Feather from 'react-native-vector-icons/Feather';
import colors from '../../../assets/styles/colors';
import AntDesign from 'react-native-vector-icons/Entypo';
import Bullets from 'react-native-easy-content-loader';

export default function Address({navigation}) {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const userId = auth().currentUser.uid;
    firestore()
      .collection('users')
      .doc(userId)
      .get()
      .then(doc => {
        const dataUser = doc.data();
        setData({...dataUser});
      })
      .then(() => setLoading(false))
      .catch(() => setLoading(false));
  });

  return (
    <Bullets
      loading={loading}
      active
      listSize={6}
      pHeight={[25]}
      pWidth={['100%']}>
      {data.address ? (
        <View style={Styles.containerAddress}>
          <View style={{margin: 20}}>
            <View style={Styles.address}>
              <Text style={Styles.title}>Thông tin địa chỉ</Text>
            </View>
            <View style={Styles.bgWhite}>
              <View style={[Styles.contact, Styles.bbw]}>
                <View style={Styles.content}>
                  <AntDesign name={'user'} size={20} color={'#B82739'} />
                  <Text style={Styles.value}>{data.address.name}</Text>
                </View>
              </View>
            </View>
            <View style={Styles.bgWhite}>
              <View style={[Styles.contact, Styles.bbw]}>
                <View style={Styles.content}>
                  <AntDesign name={'mail'} size={20} color={'#4F53B5'} />
                  <Text style={Styles.value}>{data.address.email}</Text>
                </View>
              </View>
            </View>
            <View style={Styles.bgWhite}>
              <View style={[Styles.contact, Styles.bbw]}>
                <View style={Styles.content}>
                  <AntDesign name={'phone'} size={20} color={'#03fc5a'} />
                  <Text style={Styles.value}>{data.address.phoneNumber}</Text>
                </View>
              </View>
            </View>
            <View style={Styles.bgWhite}>
              <View style={[Styles.contact, Styles.bbw]}>
                <View style={Styles.content}>
                  <AntDesign
                    name={'location-pin'}
                    size={20}
                    color={'#E90026'}
                  />
                  <Text style={Styles.value}>{data.address.address}</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              style={CartStyles.loginButton}
              onPress={() =>
                navigation.navigate('AddAddress', {
                  uid: auth().currentUser.uid,
                  add: false,
                })
              }>
              <Text style={Styles.loginButtonTitle}>Sửa</Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <View style={Styles.container}>
          <Text />
          <View style={Styles.cart}>
            <Feather name={'map-pin'} size={80} color={colors.black} />
            <Text style={Styles.cartTitleFirst}>Địa chỉ của bạn trống</Text>
            <Text style={Styles.cartTitleSecond}>
              Thêm địa chỉ vào tài khoản
            </Text>
          </View>
          <TouchableOpacity
            style={CartStyles.loginButton}
            onPress={() =>
              navigation.navigate('AddAddress', {
                uid: auth().currentUser.uid,
                add: true,
              })
            }>
            <Text style={Styles.loginButtonTitle}>Thêm ngay</Text>
          </TouchableOpacity>
        </View>
      )}
    </Bullets>
  );
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
  },
  containerAddress: {
    flex: 1,
    justifyContent: 'space-between',
    // alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: colors.white,
  },
  cart: {
    alignItems: 'center',
  },
  cartTitleFirst: {
    fontSize: 25,
    fontWeight: 'bold',
    marginTop: 20,
  },
  cartTitleSecond: {
    fontSize: 16,
    marginTop: 20,
  },
  loginButton: {
    backgroundColor: colors.eggBlue,
    width: 200,
    marginBottom: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 25,
  },
  loginButtonTitle: {
    color: colors.white,
    fontSize: 16,
    fontWeight: 'bold',
  },
  bgWhite: {
    backgroundColor: colors.white,
  },
  mt10: {
    marginTop: 10,
  },
  contact: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: 50,
    marginRight: 20,
    marginLeft: 20,
    borderBottomColor: '#999999',
  },
  bbw: {
    borderBottomWidth: 0.5,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
    color: colors.eggBlue,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  value: {
    fontSize: 16,
    color: '#5c5c5c',
    marginLeft: 20,
  },
  address: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 80,
    paddingLeft: 24,
  },
});

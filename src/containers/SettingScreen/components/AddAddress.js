import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import firestore from '@react-native-firebase/firestore';
import Bullets from 'react-native-easy-content-loader';

export default function AddAddress({route}) {
  const {uid, add} = route.params;
  const [adr, setAdr] = useState(initialValue);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!add) {
      firestore()
        .collection('users')
        .doc(uid)
        .get()
        .then(doc => {
          const newAddress = doc.data();
          setAdr({...newAddress.address, errorMessage: null});
        })
        .then(() => setLoading(false))
        .catch(() => setLoading(false));
    } else {
      setLoading(false);
    }
  }, [add, uid]);

  const handleSubmit = () => {
    const {email, name, address, phoneNumber} = adr;
    if (!email || !name || !address || !phoneNumber) {
      return setAdr(prevState => ({
        ...prevState,
        errorMessage: 'Tên, email, số điện thoại, địa chỉ không được bỏ trống',
      }));
    }

    firestore()
      .collection('users')
      .doc(uid)
      .update({
        address: {
          email: email,
          name: name,
          phoneNumber: phoneNumber,
          address: address,
        },
      })
      .then(() =>
        setAdr(prevState => ({
          ...prevState,
          errorMessage: 'Thêm địa chỉ thành công',
        })),
      )
      .then(() =>
        setTimeout(
          () =>
            setAdr(prevState => ({
              ...prevState,
              errorMessage: null,
            })),
          4000,
        ),
      )
      .catch(() => setLoading(false));
  };

  return (
    <Bullets
      loading={loading}
      active
      listSize={6}
      pHeight={[25]}
      pWidth={['100%']}>
      <View style={Styles.container}>
        <View>
          {adr.errorMessage !== null && <Text>{adr.errorMessage}</Text>}
          <TextInput
            style={Styles.textInputReg}
            autoCapitalize="none"
            onChangeText={name =>
              setAdr(prevState => ({
                ...prevState,
                name,
              }))
            }
            value={adr.name}
            placeholder={'Họ & Tên'}
          />
          <TextInput
            style={Styles.textInputReg}
            autoCapitalize="none"
            onChangeText={email =>
              setAdr(prevState => ({
                ...prevState,
                email,
              }))
            }
            value={adr.email}
            placeholder={'Email'}
          />
          <TextInput
            style={Styles.textInputReg}
            autoCapitalize="none"
            autoCompleteType={'tel'}
            dataDetectorTypes={'phoneNumber'}
            keyboardType={'numeric'}
            maxLength={10}
            textContentType={'telephoneNumber'}
            onChangeText={phoneNumber =>
              setAdr(prevState => ({
                ...prevState,
                phoneNumber,
              }))
            }
            value={adr.phoneNumber}
            placeholder={'Điện thoại'}
          />
          <TextInput
            style={Styles.textInputReg}
            autoCapitalize="none"
            onChangeText={address =>
              setAdr(prevState => ({
                ...prevState,
                address,
              }))
            }
            value={adr.address}
            placeholder={'Địa chỉ nhận hàng'}
          />
        </View>
        <TouchableOpacity
          style={Styles.loginButton}
          onPress={() => handleSubmit()}>
          <Text style={Styles.loginButtonTitle}>Lưu</Text>
        </TouchableOpacity>
      </View>
    </Bullets>
  );
}

const Styles = StyleSheet.create({
  textInputReg: {
    borderBottomWidth: 1,
    borderBottomColor: '#5c5c5c',
    marginTop: 5,
    marginBottom: 5,
    paddingLeft: -5,
    fontSize: 16,
  },
  container: {
    flex: 1,
    paddingLeft: 40,
    paddingRight: 40,
    paddingTop: 30,
    justifyContent: 'space-between',
  },
  loginButton: {
    backgroundColor: '#00c8be',
    width: '100%',
    marginTop: 20,
    padding: 12,
    alignItems: 'center',
    borderRadius: 5,
    marginBottom: 20,
  },
  loginButtonTitle: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

const initialValue = {
  email: '',
  address: '',
  name: '',
  phoneNumber: '',
  errorMessage: '',
};

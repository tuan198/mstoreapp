import React from 'react';
import {
  Modal,
  View,
  StyleSheet,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import colors from '../assets/styles/colors';

export default function modalAbout({modal, setModal}) {
  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={modal}
      onRequestClose={() => {
        setModal(!modal);
      }}>
      <View style={Styles.modalBackground}>
        <View style={Styles.activityIndicatorWrapper}>
          <View style={Styles.about}>
            <Text style={Styles.aboutTitle}>Thông tin</Text>
            <Image
              style={Styles.logoModal}
              source={require('../assets/images/logo.png')}
            />
          </View>
          <ScrollView>
            <Text style={Styles.modalContent}>
              MStore là ứng dụng thương mại điện tử, chuyên bán các sản phẩm
              laptop và phụ kiện máy tính.
            </Text>
            <Text style={Styles.modalContent}>
              Chúng tôi luôn đảm bảo mọi sản phẩm đến tay người dùng đều đạt
              chất lượng.
            </Text>
            <Text style={Styles.modalContent}>-------------------------</Text>
            <Text style={Styles.modalContent}>Tác giả: Nguyễn Minh Tuấn</Text>
            <Text style={Styles.modalContent}>Phiên bản: v1.0.13</Text>
          </ScrollView>
          <TouchableOpacity
            style={Styles.btnCancel}
            onPress={() => {
              setModal(!modal);
            }}>
            <Text style={Styles.cancelTitle}>Thoát</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}

const Styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: '50%',
    width: '80%',
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  about: {
    marginTop: 20,
    display: 'flex',
    alignItems: 'center',
  },
  aboutTitle: {
    marginBottom: 20,
    fontSize: 16,
    fontWeight: 'bold',
  },
  logoModal: {
    width: 160,
    height: 60,
    marginBottom: 10,
  },
  modalContent: {
    marginLeft: 20,
    marginRight: 20,
    fontSize: 14,
    lineHeight: 22,
  },
  btnCancel: {
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#dadada',
    width: '100%',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#dedede',
    borderBottomEndRadius: 5,
    borderBottomStartRadius: 5,
  },
  cancelTitle: {
    color: colors.google,
    fontSize: 18,
    fontWeight: 'bold',
  },
});

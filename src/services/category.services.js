import firestore from '@react-native-firebase/firestore';

const productsRef = firestore().collection('products');

export const getCategories = async category => {
  const querySnapshot = await productsRef
    .where('categories', 'array-contains', category)
    .limit(10)
    .get();
  const categories = [];
  querySnapshot.forEach(doc => {
    categories.push({
      id: doc.id,
      name: doc.data().name,
      price: doc.data().price,
      oldPrice: doc.data().oldPrice,
      image: doc.data().productImage,
    });
  });

  return categories;
};

export const getCategoryList = async category => {
  const querySnapshot = await productsRef
    .where('categories', 'array-contains', category)
    .get();
  const categories = [];
  querySnapshot.forEach(doc => {
    categories.push({
      id: doc.id,
      name: doc.data().name,
      price: doc.data().price,
      oldPrice: doc.data().oldPrice,
      image: doc.data().productImage,
    });
  });

  return categories;
};

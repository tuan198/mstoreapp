import firestore from '@react-native-firebase/firestore';

const productsRef = firestore().collection('products');
const trademarksRef = firestore().collection('trademarks');

export const getTrademarks = async () => {
  const querySnapshot = await trademarksRef.get();
  const trademarks = [];
  querySnapshot.forEach(doc => {
    trademarks.push({
      id: doc.id,
      name: doc.data().name,
      image: doc.data().image,
      description: doc.data().description,
    });
  });

  return trademarks;
};

export const getTrademarkList = async trademark => {
  const querySnapshot = await productsRef
    .where('trademark', '==', trademark)
    .get();
  const trademarks = [];
  querySnapshot.forEach(doc => {
    trademarks.push({
      id: doc.id,
      name: doc.data().name,
      price: doc.data().price,
      oldPrice: doc.data().oldPrice,
      image: doc.data().productImage,
    });
  });

  return trademarks;
};

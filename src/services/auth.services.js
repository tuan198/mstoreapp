import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

export const loginRequest = async ({email, password}) => {
  await auth().signInWithEmailAndPassword(email, password);
};

export const loginFbRequest = async () => {};

export const loginGgRequest = async () => {};

export const signUpRequest = async ({email, password, data}) => {
  const {displayName, photoURL, phoneNumber, gender} = data;
  await auth()
    .createUserWithEmailAndPassword(email, password)
    .then(userCredentials => {
      return userCredentials.user.updateProfile({
        displayName,
        photoURL,
        phoneNumber,
      });
    })
    .then(() => {
      const userId = auth().currentUser.uid;
      const getUser = auth().currentUser;
      firestore()
        .collection('users')
        .doc(userId)
        .set({
          role: 'USER',
          uid: userId,
          status: 'ACTIVE',
          displayName: getUser.displayName,
          email,
          password,
          phoneNumber,
          gender,
          photoURL: getUser.photoURL,
          providerId: getUser.providerId,
          metadata: {
            creationTime: new Date(getUser.metadata.creationTime).toString(),
            lastSignInTime: new Date(
              getUser.metadata.lastSignInTime,
            ).toString(),
          },
        });
    });
};

export const postUser = async () => {
  try {
    const userId = auth().currentUser.uid;
    const getUser = auth().currentUser;
    const providerData = auth().currentUser.providerData[0];
    await firestore()
      .collection('users')
      .doc(userId)
      .collection('infomation')
      .doc(userId)
      .set({
        displayName: providerData.displayName,
        email: providerData.email,
        isAnonymous: getUser.isAnonymous,
        metadata: {
          creationTime: new Date(getUser.metadata.creationTime).toString(),
          lastSignInTime: new Date(getUser.metadata.lastSignInTime).toString(),
        },
        phoneNumber: providerData.phoneNumber,
        photoURL: providerData.photoURL,
        providerData: [
          {
            providerId: providerData.providerId,
            uid: providerData.uid,
          },
        ],
        providerId: providerData.providerId,
        uid: getUser.uid,
      });
    return 'created an user';
  } catch (e) {
    return e.message;
  }
};

export const getCurrentUser = async () => {
  var data = null;
  await auth().onAuthStateChanged(userChange => {
    data = userChange;
  });
  return data;
};

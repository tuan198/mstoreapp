import firestore from '@react-native-firebase/firestore';

const productsRef = firestore().collection('products');

export const getProducts = async () => {
  const querySnapshot = await productsRef.limit(10).get();
  const products = [];
  querySnapshot.forEach(doc => {
    products.push({
      id: doc.id,
      name: doc.data().name,
      price: doc.data().price,
      oldPrice: doc.data().oldPrice,
      image: doc.data().productImage,
    });
  });

  return products;
};

export const getProductList = async () => {
  const querySnapshot = await productsRef.get();
  const products = [];
  querySnapshot.forEach(doc => {
    products.push({
      id: doc.id,
      name: doc.data().name,
      price: doc.data().price,
      oldPrice: doc.data().oldPrice,
      image: doc.data().productImage,
    });
  });

  return products;
};

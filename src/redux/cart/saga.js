import {all, takeEvery, put, fork, call, take} from 'redux-saga/effects';

import actions from './actions';
import {cartServices} from '../../services/cart.services';

export function* getCarts() {
  yield takeEvery(actions.GET_CART_REQUEST, function*({payload}) {
    const {userId} = payload;
    try {
      const carts = yield call(cartServices.getCarts, {userId});
      console.log(carts.docs);
      yield put({type: actions.GET_CART_SUCCESS, carts: carts});
    } catch (error) {
      yield put({type: actions.GET_CART_ERROR, error});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getCarts)]);
}

import actions from './actions';

const initialState = {
  carts: [],
  loading: true,
  error: null,
};

export default function cartReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_CART_REQUEST:
      return {
        ...state,
      };
    case actions.GET_CART_SUCCESS:
      return {
        ...state,
        loading: false,
        carts: action.carts,
      };
    case actions.GET_CART_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

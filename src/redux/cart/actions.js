const actions = {
  GET_CART_REQUEST: 'GET_CART_REQUEST',
  GET_CART_SUCCESS: 'GET_CART_SUCCESS',
  GET_CART_ERROR: 'GET_CART_ERROR',

  getCart: payload => ({
    type: actions.GET_CART_REQUEST,
    payload: payload,
  }),
};

export default actions;

import {all, takeEvery, put, fork, call} from 'redux-saga/effects';
import * as Services from '../../services/trademark.services';
import actions from './actions';

export function* getTrademark() {
  yield takeEvery(actions.GET_TRADEMARKS_REQUEST, function*() {
    try {
      const trademarks = yield call(Services.getTrademarks);
      yield put({type: actions.GET_TRADEMARKS_SUCCESS, trademarks: trademarks});
    } catch (e) {
      yield put({type: actions.GET_TRADEMARKS_ERROR, error: e.message});
    }
  });
}

export function* getTrademarkList() {
  yield takeEvery(actions.GET_TRADEMARK_LIST_REQUEST, function*({payload}) {
    const {trademark} = payload;
    try {
      const trademarkList = yield call(Services.getTrademarkList, trademark);
      yield put({
        type: actions.GET_TRADEMARK_LIST_SUCCESS,
        trademarkList: trademarkList,
      });
    } catch (e) {
      yield put({type: actions.GET_TRADEMARK_LIST_ERROR, error: e.message});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getTrademark)]);
  yield all([fork(getTrademarkList)]);
}

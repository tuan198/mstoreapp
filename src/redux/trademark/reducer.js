import actions from './actions';

const initialState = {
  trademarks: [],
  trademarkList: [],
  trademarkLoading: false,
  error: null,
};

export default function trademarkReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_TRADEMARKS_REQUEST:
      return {
        ...state,
        trademarkLoading: true,
      };
    case actions.GET_TRADEMARKS_SUCCESS:
      return {
        ...state,
        trademarks: action.trademarks,
        trademarkLoading: false,
      };
    case actions.GET_TRADEMARKS_ERROR:
      return {
        ...state,
        trademarks: [],
        error: action.error,
        trademarkLoading: false,
      };
    case actions.GET_TRADEMARK_LIST_REQUEST:
      return {
        ...state,
        trademarkLoading: true,
      };
    case actions.GET_TRADEMARK_LIST_SUCCESS:
      return {
        ...state,
        trademarkList: action.trademarkList,
        trademarkLoading: false,
      };
    case actions.GET_TRADEMARK_LIST_ERROR:
      return {
        ...state,
        trademarkList: [],
        error: action.error,
        trademarkLoading: false,
      };
    default:
      return state;
  }
}

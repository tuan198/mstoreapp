import {combineReducers} from 'redux';
import Auth from './auth/reducer';
import Cart from './cart/reducer';
import Product from './product/reducer';
import Category from './category/reducer';
import Trademark from './trademark/reducer';

export default combineReducers({
  Auth,
  Cart,
  Category,
  Product,
  Trademark,
});

const actions = {
  GET_CATEGORIES_REQUEST: 'GET_CATEGORIES_REQUEST',
  GET_CATEGORIES_SUCCESS: 'GET_CATEGORIES_SUCCESS',
  GET_CATEGORIES_ERROR: 'GET_CATEGORIES_ERROR',

  GET_CATEGORY_LIST_REQUEST: 'GET_CATEGORY_LIST_REQUEST',
  GET_CATEGORY_LIST_SUCCESS: 'GET_CATEGORY_LIST_SUCCESS',
  GET_CATEGORY_LIST_ERROR: 'GET_CATEGORY_LIST_ERROR',

  getCategories: payload => ({
    type: actions.GET_CATEGORIES_REQUEST,
    payload: payload,
  }),

  getCategoryList: payload => ({
    type: actions.GET_CATEGORY_LIST_REQUEST,
    payload: payload,
  }),
};

export default actions;

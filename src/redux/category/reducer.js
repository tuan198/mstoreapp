import actions from './actions';

const initialState = {
  categories: [],
  categoryList: [],
  categoryLoading: false,
  error: null,
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_CATEGORIES_REQUEST:
      return {
        ...state,
        categoryLoading: true,
      };
    case actions.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        categories: action.categories,
        categoryLoading: false,
      };
    case actions.GET_CATEGORIES_ERROR:
      return {
        ...state,
        categories: [],
        error: action.error,
        categoryLoading: false,
      };
    case actions.GET_CATEGORY_LIST_REQUEST:
      return {
        ...state,
        categoryLoading: true,
      };
    case actions.GET_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        categoryList: action.categoryList,
        categoryLoading: false,
      };
    case actions.GET_CATEGORY_LIST_ERROR:
      return {
        ...state,
        categoryList: [],
        error: action.error,
        categoryLoading: false,
      };
    default:
      return state;
  }
}

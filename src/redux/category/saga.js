import {all, takeEvery, put, fork, call} from 'redux-saga/effects';
import * as Services from '../../services/category.services';
import actions from './actions';

export function* getCategories() {
  yield takeEvery(actions.GET_CATEGORIES_REQUEST, function*({payload}) {
    const {category} = payload;
    try {
      const categories = yield call(Services.getCategories, category);
      yield put({type: actions.GET_CATEGORIES_SUCCESS, categories: categories});
    } catch (e) {
      yield put({type: actions.GET_CATEGORIES_ERROR, error: e.message});
    }
  });
}

export function* getCategoryList() {
  yield takeEvery(actions.GET_CATEGORY_LIST_REQUEST, function*({payload}) {
    const {category} = payload;
    try {
      const categoryList = yield call(Services.getCategoryList, category);
      yield put({
        type: actions.GET_CATEGORY_LIST_SUCCESS,
        categoryList: categoryList,
      });
    } catch (e) {
      yield put({type: actions.GET_CATEGORY_LIST_ERROR, error: e.message});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getCategories)]);
  yield all([fork(getCategoryList)]);
}

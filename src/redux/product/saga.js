import {all, takeEvery, put, fork, call} from 'redux-saga/effects';
import * as Services from '../../services/product.services';
import actions from './actions';

export function* getProducts() {
  yield takeEvery(actions.GET_PRODUCTS_REQUEST, function*() {
    try {
      const products = yield call(Services.getProducts);
      yield put({type: actions.GET_PRODUCTS_SUCCESS, products: products});
    } catch (e) {
      yield put({type: actions.GET_PRODUCTS_ERROR, error: e.message});
    }
  });
}

export function* getProductList() {
  yield takeEvery(actions.GET_PRODUCT_LIST_REQUEST, function*() {
    try {
      const productList = yield call(Services.getProductList);
      yield put({
        type: actions.GET_PRODUCT_LIST_SUCCESS,
        productList: productList,
      });
    } catch (e) {
      yield put({type: actions.GET_PRODUCT_LIST_ERROR, error: e.message});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getProducts)]);
  yield all([fork(getProductList)]);
}

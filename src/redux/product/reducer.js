import actions from './actions';

const initialState = {
  products: [],
  productList: [],
  productLoading: false,
  error: null,
};

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case actions.GET_PRODUCTS_REQUEST:
      return {
        ...state,
        productLoading: true,
      };
    case actions.GET_PRODUCTS_SUCCESS:
      return {
        ...state,
        products: action.products,
        productLoading: false,
      };
    case actions.GET_PRODUCTS_ERROR:
      return {
        ...state,
        products: [],
        error: action.error,
        productLoading: false,
      };
    case actions.GET_PRODUCT_LIST_REQUEST:
      return {
        ...state,
        productLoading: true,
      };
    case actions.GET_PRODUCT_LIST_SUCCESS:
      return {
        ...state,
        productList: action.productList,
        productLoading: false,
      };
    case actions.GET_PRODUCT_LIST_ERROR:
      return {
        ...state,
        productList: [],
        error: action.error,
        productLoading: false,
      };
    default:
      return state;
  }
}

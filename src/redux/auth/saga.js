import {all, takeEvery, put, fork, call} from 'redux-saga/effects';
import * as Services from '../../services/auth.services';

import actions from './actions';

export function* LoginRequest() {
  yield takeEvery(actions.LOGIN_REQUEST, function*({payload}) {
    const {email, password, resolve, reject} = payload;
    try {
      yield call(Services.loginRequest, {
        email,
        password,
      });
      yield put({type: actions.REGISTER_SUCCESS});
      yield resolve();
    } catch (error) {
      yield put({type: actions.LOGIN_ERROR, error});
      yield reject();
    }
  });
}

export function* LoginFbRequest() {
  yield takeEvery(actions.LOGIN_FACEBOOK_REQUEST, function*({payload}) {
    const {resolve, reject} = payload;
    try {
      yield call(Services.loginFbRequest);
      yield put({type: actions.LOGIN_FACEBOOK_SUCCESS});
      yield resolve();
    } catch (error) {
      yield put({type: actions.LOGIN_FACEBOOK_ERROR, error});
      yield reject();
    }
  });
}

export function* LoginGgRequest() {
  yield takeEvery(actions.LOGIN_GOOGLE_REQUEST, function*({payload}) {
    const {resolve, reject} = payload;
    try {
      yield call(Services.loginGgRequest);
      yield put({type: actions.LOGIN_GOOGLE_SUCCESS});
      yield resolve();
    } catch (error) {
      yield put({type: actions.LOGIN_GOOGLE_ERROR, error});
      yield reject();
    }
  });
}

export function* signUpRequest() {
  yield takeEvery(actions.REGISTER_REQUEST, function*({payload}) {
    const {email, password, data, resolve, reject} = payload;
    try {
      yield call(Services.signUpRequest, {email, password, data});
      yield put({type: actions.REGISTER_SUCCESS});
      yield resolve();
    } catch (e) {
      yield put({type: actions.REGISTER_ERROR, error: e.message});
      yield reject();
    }
  });
}

export function* checkAuthorization() {
  const data = yield call(Services.getCurrentUser);
  if (data) {
    yield put({type: actions.CHECK_AUTHORIZATION});
  } else {
    yield put({type: actions.CHECK_AUTHORIZATION_ERROR});
  }
}

export function* SignOut() {
  yield takeEvery(actions.LOGOUT, function*() {});
}

export default function* rootSaga() {
  yield all([
    fork(LoginRequest),
    fork(SignOut),
    fork(checkAuthorization),
    fork(signUpRequest),
    fork(LoginFbRequest),
    fork(LoginGgRequest),
  ]);
}

import actions from './actions';

const initialState = {
  userData: [],
  isLoggedIn: false,
  loading: false,
  error: null,
};

export default function authReducer(state = initialState, action) {
  switch (action.type) {
    case actions.CHECK_AUTHORIZATION:
      return {
        ...state,
        loading: false,
        isLoggedIn: true,
      };
    case actions.CHECK_AUTHORIZATION_ERROR:
      return {
        ...state,
        error: action.error,
        loading: false,
        isLoggedIn: false,
      };
    case actions.LOGIN_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
        isLoggedIn: false,
      };
    case actions.LOGIN_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        isLoggedIn: true,
      };
    case actions.LOGIN_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        loading: false,
        error: action.error,
      };

    case actions.LOGIN_FACEBOOK_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
        isLoggedIn: false,
      };
    case actions.LOGIN_FACEBOOK_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        isLoggedIn: true,
      };
    case actions.LOGIN_FACEBOOK_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        loading: false,
        error: action.error,
      };
    case actions.LOGIN_GOOGLE_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
        isLoggedIn: false,
      };
    case actions.LOGIN_GOOGLE_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        isLoggedIn: true,
      };
    case actions.LOGIN_GOOGLE_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        loading: false,
        error: action.error,
      };

    case actions.REGISTER_REQUEST:
      return {
        ...state,
        error: null,
        loading: true,
        isLoggedIn: false,
      };
    case actions.REGISTER_SUCCESS:
      return {
        ...state,
        error: null,
        loading: false,
        isLoggedIn: true,
      };
    case actions.REGISTER_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        loading: false,
        error: action.error,
      };
    case actions.LOGOUT:
      return {
        ...initialState,
      };
    default:
      return state;
  }
}

const actions = {
  CHECK_AUTHORIZATION: 'CHECK_AUTHORIZATION',
  CHECK_AUTHORIZATION_ERROR: 'CHECK_AUTHORIZATION_ERROR',

  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_ERROR: 'LOGIN_ERROR',

  LOGIN_FACEBOOK_REQUEST: 'LOGIN_FACEBOOK_REQUEST',
  LOGIN_FACEBOOK_SUCCESS: 'LOGIN_FACEBOOK_SUCCESS',
  LOGIN_FACEBOOK_ERROR: 'LOGIN_FACEBOOK_ERROR',

  LOGIN_GOOGLE_REQUEST: 'LOGIN_GOOGLE_REQUEST',
  LOGIN_GOOGLE_SUCCESS: 'LOGIN_GOOGLE_SUCCESS',
  LOGIN_GOOGLE_ERROR: 'LOGIN_GOOGLE_ERROR',

  LOGOUT: 'LOGOUT',

  REGISTER_REQUEST: 'REGISTER_REQUEST',
  REGISTER_SUCCESS: 'REGISTER_SUCCESS',
  REGISTER_ERROR: 'REGISTER_ERROR',

  FORGET_PASSWORD_REQUEST: 'FORGET_PASSWORD_REQUEST',

  RESET_PASSWORD_REQUEST: 'RESET_PASSWORD_REQUEST',
  RESET_PASSWORD_SUCCESS: 'RESET_PASSWORD_SUCCESS',
  RESET_PASSWORD_ERROR: 'RESET_PASSWORD_ERROR',

  checkAuthorization: () => ({
    type: actions.CHECK_AUTHORIZATION,
  }),

  login: payload => ({
    type: actions.LOGIN_REQUEST,
    payload: payload,
  }),

  loginFacebook: () => ({type: actions.LOGIN_FACEBOOK_REQUEST}),

  loginGoogle: () => ({type: actions.LOGIN_GOOGLE_REQUEST}),

  forgotPassword: payload => ({
    type: actions.FORGET_PASSWORD_REQUEST,
    payload: payload,
  }),

  register: payload => ({
    type: actions.REGISTER_REQUEST,
    payload: payload,
  }),

  logout: () => ({
    type: actions.LOGOUT,
  }),

  resetPassword: payload => ({
    type: actions.RESET_PASSWORD_REQUEST,
    payload: payload,
  }),
};

export default actions;

import {all} from 'redux-saga/effects';
import authSagas from './auth/saga';
import cartSagas from './cart/saga';
import productSagas from './product/saga';
import categorySagas from './category/saga';
import trademarkSagas from './trademark/saga';

export default function* rootSaga() {
  yield all([
    authSagas(),
    cartSagas(),
    productSagas(),
    categorySagas(),
    trademarkSagas(),
  ]);
}

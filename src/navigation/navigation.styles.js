import {StyleSheet} from 'react-native';

export const StackStyles = StyleSheet.create({
  drawer: {
    marginLeft: 10,
  },
  imageStyle: {
    width: 80,
    height: 30,
  },
});

export const DrawerStyles = StyleSheet.create({
  profile: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    marginBottom: 20,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 20,
  },
  imageStyle: {
    width: 80,
    height: 80,
    borderRadius: 50,
    marginBottom: 10,
  },
  nameStyle: {
    color: '#5c5c5c',
    fontWeight: 'bold',
    fontSize: 18,
  },
  emailStyle: {
    color: '#5c5c5c',
    fontWeight: 'bold',
    fontSize: 12,
  },
  imageStyleStack: {
    width: 80,
    height: 30,
  },
  drawer: {
    marginLeft: 10,
  },
  imageStyleDrawer: {
    width: 80,
    height: 30,
  },
  title: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  titleText: {
    marginLeft: 10,
    color: '#5c5c5c',
  },
});

import React, {useEffect, useState} from 'react';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';
import {createStackNavigator} from '@react-navigation/stack';
import {View, Text, Image, Alert} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import auth from '@react-native-firebase/auth';

import MemberTabNavigator from './MemberTabNavigator';
import Profile from '../containers/SettingScreen/components/Profile';
import SettingMenu from '../containers/SettingScreen/components/SettingMenu';
import ContactUs from '../containers/SettingScreen/components/ContactUs';
import ChangePassword from '../containers/SettingScreen/components/SettingMenu/ChangePassword';
import ForgotPassword from '../containers/AuthScreen/ForgotPassword';

import {DrawerStyles} from './navigation.styles';
import AntDesign from 'react-native-vector-icons/AntDesign';

const DrawerStack = createStackNavigator();

function SettingDrawer({navigation}) {
  return (
    <DrawerStack.Navigator>
      <DrawerStack.Screen
        name="SettingMenu"
        component={SettingMenu}
        options={{
          headerTitle: () => (
            <Image
              source={require('../assets/images/logo.png')}
              style={DrawerStyles.imageStyleDrawer}
            />
          ),
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={DrawerStyles.drawer}>
              <AntDesign name={'bars'} color={'#5c5c5c'} size={25} />
            </TouchableOpacity>
          ),
        }}
      />
      <DrawerStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{
          headerTitle: () => (
            <Image
              source={require('../assets/images/logo.png')}
              style={DrawerStyles.imageStyleDrawer}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <DrawerStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{
          headerTitle: () => (
            <Image
              source={require('../assets/images/logo.png')}
              style={DrawerStyles.imageStyleDrawer}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
    </DrawerStack.Navigator>
  );
}

function ProfileDrawer({navigation}) {
  return (
    <DrawerStack.Navigator>
      <DrawerStack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerTitle: () => (
            <Image
              source={require('../assets/images/logo.png')}
              style={DrawerStyles.imageStyleDrawer}
            />
          ),
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={DrawerStyles.drawer}>
              <AntDesign name={'bars'} color={'#5c5c5c'} size={25} />
            </TouchableOpacity>
          ),
        }}
      />
    </DrawerStack.Navigator>
  );
}

function ContactDrawer({navigation}) {
  return (
    <DrawerStack.Navigator>
      <DrawerStack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{
          headerTitle: () => (
            <Image
              source={require('../assets/images/logo.png')}
              style={DrawerStyles.imageStyleDrawer}
            />
          ),
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={DrawerStyles.drawer}>
              <AntDesign name={'bars'} color={'#5c5c5c'} size={25} />
            </TouchableOpacity>
          ),
        }}
      />
    </DrawerStack.Navigator>
  );
}

const Drawer = createDrawerNavigator();

function CustomDrawerContent(props, navigation, user) {
  const userDisplay = auth().currentUser;

  const handleSignOut = () => {
    auth()
      .signOut()
      .then(() => {
        navigation.closeDrawer();
      });
  };

  return (
    <DrawerContentScrollView {...props}>
      {user ? (
        <View style={DrawerStyles.profile}>
          <Image
            source={{
              uri: userDisplay.providerData[0].photoURL,
            }}
            style={DrawerStyles.imageStyle}
          />
          <Text style={DrawerStyles.nameStyle}>
            {userDisplay.providerData[0].displayName}
          </Text>
          <Text style={DrawerStyles.emailStyle}>
            {userDisplay.providerData[0].email}
          </Text>
        </View>
      ) : (
        <TouchableOpacity
          onPress={() => navigation.navigate('SignIn')}
          style={DrawerStyles.profile}>
          <Image
            source={require('../assets/images/logoRn.png')}
            style={DrawerStyles.imageStyle}
          />
          <Text style={DrawerStyles.nameStyle}>Đăng nhập</Text>
          <Text style={DrawerStyles.emailStyle}>Bạn chưa đăng nhập.</Text>
        </TouchableOpacity>
      )}
      <DrawerItemList {...props} />
      <DrawerItem
        label={() => (
          <View style={DrawerStyles.title}>
            <AntDesign
              name={'exclamationcircleo'}
              color={'#5c5c5c'}
              size={20}
            />
            <Text style={DrawerStyles.titleText}>Thông tin ứng dụng</Text>
          </View>
        )}
        onPress={() =>
          Alert.alert(
            'Thông tin',
            '\nMStore là ứng dụng thương mại điện tử, chuyên bán các sản phẩm laptop và phụ kiện máy tính.' +
              '\n\nChúng tôi luôn đảm bảo mọi sản phẩm đến tay người dùng đều đạt chất lượng.\n\n' +
              '-------------------------\n\n' +
              'Tác giả: Nguyễn Minh Tuấn\n\n' +
              'Phiên bản: v1.0.13',
            [
              {
                text: 'Thoát',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
            ],
          )
        }
      />
      {user ? (
        <DrawerItem
          label={() => (
            <View style={DrawerStyles.title}>
              <AntDesign name={'logout'} color={'#5c5c5c'} size={20} />
              <Text style={DrawerStyles.titleText}>Đăng xuất</Text>
            </View>
          )}
          onPress={() =>
            Alert.alert(
              'Đăng xuất',
              'Bạn có muốn đăng xuất khỏi ứng dụng không?',
              [
                {
                  text: 'Không',
                  onPress: () => console.log('Cancel Pressed'),
                  style: 'cancel',
                },
                {text: 'Có', onPress: handleSignOut},
              ],
              {cancelable: false},
            )
          }
        />
      ) : null}
    </DrawerContentScrollView>
  );
}

export default function DrawerNavigator({navigation}) {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();

  // eslint-disable-next-line react-hooks/exhaustive-deps,no-shadow
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  if (initializing) {
    return null;
  }

  return (
    <Drawer.Navigator
      initialRouteName="HomeScreen"
      drawerContent={props => CustomDrawerContent(props, navigation, user)}>
      <Drawer.Screen
        name="HomeScreen"
        component={MemberTabNavigator}
        options={{
          title: () => (
            <View style={DrawerStyles.title}>
              <AntDesign name={'home'} color={'#5c5c5c'} size={20} />
              <Text style={DrawerStyles.titleText}>Trang chủ</Text>
            </View>
          ),
        }}
      />
      {user && (
        <Drawer.Screen
          name="Profile"
          component={ProfileDrawer}
          options={{
            title: () => (
              <View style={DrawerStyles.title}>
                <AntDesign name={'user'} color={'#5c5c5c'} size={20} />
                <Text style={DrawerStyles.titleText}>Hồ sơ</Text>
              </View>
            ),
          }}
        />
      )}
      <Drawer.Screen
        name="ContactUs"
        component={ContactDrawer}
        options={{
          title: () => (
            <View style={DrawerStyles.title}>
              <AntDesign name={'phone'} color={'#5c5c5c'} size={20} />
              <Text style={DrawerStyles.titleText}>Liên hệ</Text>
            </View>
          ),
        }}
      />
      <Drawer.Screen
        name="SettingMenu"
        component={SettingDrawer}
        options={{
          title: () => (
            <View style={DrawerStyles.title}>
              <AntDesign name={'setting'} color={'#5c5c5c'} size={20} />
              <Text style={DrawerStyles.titleText}>Cài đặt</Text>
            </View>
          ),
        }}
      />
    </Drawer.Navigator>
  );
}

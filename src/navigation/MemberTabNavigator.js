import React, {useEffect, useState} from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import AntDesign from 'react-native-vector-icons/AntDesign';

import HomeStackNavigator from './StackNavigator/HomeStackNavigator';
import CartStackNavigator from './StackNavigator/CartStackNavigator';
import SettingStackNavigator from './StackNavigator/SettingStackNavigator';
import SearchStackNavigator from './StackNavigator/SearchStackNavigator';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

const Tab = createMaterialBottomTabNavigator();

export default function MemberTabNavigator() {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState();
  const [total, setTotal] = useState(0);

  // eslint-disable-next-line react-hooks/exhaustive-deps,no-shadow
  const onAuthStateChanged = user => {
    setUser(user);
    if (initializing) {
      setInitializing(false);
    }
  };

  useEffect(() => {
    return auth().onAuthStateChanged(onAuthStateChanged); // unsubscribe on unmount
  }, [onAuthStateChanged]);

  useEffect(() => {
    if (user) {
      const List = [];
      const userId = auth().currentUser.uid;
      firestore()
        .collection('shopping-card')
        .doc(userId)
        .collection('products')
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach(documentSnapshot => {
            const data = documentSnapshot.data();
            List.push(data.quantum);
          });
        })
        .then(() => setTotal(List.reduce((a, b) => a + b)));
    }
  });

  if (initializing) {
    return null;
  }

  return (
    <Tab.Navigator
      initialRouteName="HomeScreen"
      activeColor="#00c8be"
      inactiveColor="#00c8be"
      barStyle={{backgroundColor: '#ffffff'}}>
      <Tab.Screen
        name="HomeScreen"
        component={HomeStackNavigator}
        options={{
          title: 'Trang chủ',
          tabBarIcon: ({focused}) => (
            <AntDesign
              name="home"
              color={focused ? '#00c8be' : '#5c5c5c'}
              size={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name="SearchScreen"
        component={SearchStackNavigator}
        options={{
          title: 'Tìm kiếm',
          tabBarIcon: ({focused}) => (
            <AntDesign
              name="search1"
              color={focused ? '#00c8be' : '#5c5c5c'}
              size={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name="CartScreen"
        component={CartStackNavigator}
        options={{
          title: 'Giỏ hàng',
          tabBarBadge: user ? total : false,
          tabBarIcon: ({focused}) => (
            <AntDesign
              name="shoppingcart"
              color={focused ? '#00c8be' : '#5c5c5c'}
              size={25}
            />
          ),
        }}
      />
      <Tab.Screen
        name="SettingScreen"
        component={SettingStackNavigator}
        options={{
          title: 'Cài đặt',
          tabBarIcon: ({focused}) => (
            <AntDesign
              name="user"
              color={focused ? '#00c8be' : '#5c5c5c'}
              size={25}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}

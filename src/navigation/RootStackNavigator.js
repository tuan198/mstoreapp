import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import DrawerNavigator from './DrawerNavigator';

const RootStack = createStackNavigator();

const RootStackScreen = () => (
  <RootStack.Navigator initialRouteName="App" headerMode="none">
    <RootStack.Screen
      name="App"
      component={DrawerNavigator}
      options={{
        animationEnabled: false,
      }}
    />
  </RootStack.Navigator>
);

export default RootStackScreen;

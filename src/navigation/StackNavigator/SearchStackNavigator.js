import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SearchScreen from '../../containers/SearchScreen';
import HomeDetail from '../../containers/HomeScreen/components/Detail';
import AuthStackNavigator from './AuthStackNavigator';
import Checkout from '../../containers/CartScreen/components/Checkout';

import {Image} from 'react-native';
import {StackStyles} from '../navigation.styles';

const SearchStack = createStackNavigator();

export default function SearchStackNavigator() {
  return (
    <SearchStack.Navigator>
      <SearchStack.Screen
        name="SearchScreen"
        component={SearchScreen}
        options={{headerShown: false}}
      />
      <SearchStack.Screen
        name="HomeDetail"
        component={HomeDetail}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SearchStack.Screen
        name="SignIn"
        component={AuthStackNavigator}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SearchStack.Screen
        name="Checkout"
        component={Checkout}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
    </SearchStack.Navigator>
  );
}

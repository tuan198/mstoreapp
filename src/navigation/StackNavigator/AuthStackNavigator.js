import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SignIn from '../../containers/AuthScreen/SignIn';
import SignUp from '../../containers/AuthScreen/SignUp';
import ForgotPassword from '../../containers/AuthScreen/ForgotPassword';

const AuthStack = createStackNavigator();

const AuthStackNavigator = () => (
  <AuthStack.Navigator initialRouteName="SignIn">
    <AuthStack.Screen
      name="SignIn"
      component={SignIn}
      options={{headerShown: false}}
    />
    <AuthStack.Screen
      name="SignUp"
      component={SignUp}
      options={{headerShown: false}}
    />
    <AuthStack.Screen
      name="ForgotPassword"
      component={ForgotPassword}
      options={{headerShown: false}}
    />
  </AuthStack.Navigator>
);

export default AuthStackNavigator;

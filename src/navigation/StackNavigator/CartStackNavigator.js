import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import CartScreen from '../../containers/CartScreen';
import AuthStackNavigator from './AuthStackNavigator';
import Checkout from '../../containers/CartScreen/components/Checkout';
import Address from '../../containers/SettingScreen/components/Address';
import HomeDetail from '../../containers/HomeScreen/components/Detail';

import {Image} from 'react-native';
import {StackStyles} from '../navigation.styles';

const CartStack = createStackNavigator();

export default function CartStackNavigator() {
  return (
    <CartStack.Navigator>
      <CartStack.Screen
        name="CartScreen"
        component={CartScreen}
        options={{
          headerTitle: 'Giỏ hàng',
          headerTitleAlign: 'center',
        }}
      />
      <CartStack.Screen
        name="SignIn"
        component={AuthStackNavigator}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <CartStack.Screen
        name="Checkout"
        component={Checkout}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <CartStack.Screen
        name="HomeDetail"
        component={HomeDetail}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <CartStack.Screen
        name="Address"
        component={Address}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
    </CartStack.Navigator>
  );
}

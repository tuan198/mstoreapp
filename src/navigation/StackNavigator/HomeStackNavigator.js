import * as React from 'react';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Image} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {createStackNavigator} from '@react-navigation/stack';

import HomeScreen from '../../containers/HomeScreen';
import HomeDetail from '../../containers/HomeScreen/components/Detail';
import ProductList from '../../containers/HomeScreen/components/ProductList';
import Trademarks from '../../containers/HomeScreen/components/Trademarks';
import AuthStackNavigator from './AuthStackNavigator';

import {StackStyles} from '../navigation.styles';

const HomeStack = createStackNavigator();

export default function HomeStackNavigator({navigation}) {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={StackStyles.drawer}>
              <AntDesign name={'bars'} color={'#5c5c5c'} size={25} />
            </TouchableOpacity>
          ),
        }}
      />
      <HomeStack.Screen
        name="HomeDetail"
        component={HomeDetail}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <HomeStack.Screen
        name="ProductList"
        component={ProductList}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <HomeStack.Screen
        name="Trademarks"
        component={Trademarks}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <HomeStack.Screen
        name="SignIn"
        component={AuthStackNavigator}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
    </HomeStack.Navigator>
  );
}

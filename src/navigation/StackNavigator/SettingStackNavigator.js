import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import SettingScreen from '../../containers/SettingScreen';
import Orders from '../../containers/SettingScreen/components/Orders';
import Address from '../../containers/SettingScreen/components/Address';
import AddAddress from '../../containers/SettingScreen/components/AddAddress';
import Pay from '../../containers/SettingScreen/components/Pay';
import Profile from '../../containers/SettingScreen/components/Profile';
import SettingMenu from '../../containers/SettingScreen/components/SettingMenu';
import ContactUs from '../../containers/SettingScreen/components/ContactUs';
import ChangePassword from '../../containers/SettingScreen/components/SettingMenu/ChangePassword';
import ForgotPassword from '../../containers/AuthScreen/ForgotPassword';
import AuthStackNavigator from './AuthStackNavigator';
import {Image, StyleSheet} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {StackStyles} from '../navigation.styles';

const SettingStack = createStackNavigator();

export default function SettingStackNavigator({navigation}) {
  return (
    <SettingStack.Navigator>
      <SettingStack.Screen
        name="SettingScreen"
        component={SettingScreen}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
          headerLeft: () => (
            <TouchableOpacity
              onPress={() => navigation.openDrawer()}
              style={Styles.drawer}>
              <AntDesign name={'bars'} color={'#5c5c5c'} size={25} />
            </TouchableOpacity>
          ),
        }}
      />
      <SettingStack.Screen
        name="Profile"
        component={Profile}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="Orders"
        component={Orders}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="Address"
        component={Address}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="AddAddress"
        component={AddAddress}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="Pay"
        component={Pay}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="SettingMenu"
        component={SettingMenu}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="ContactUs"
        component={ContactUs}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="ChangePassword"
        component={ChangePassword}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={Styles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
      <SettingStack.Screen
        name="SignIn"
        component={AuthStackNavigator}
        options={{
          headerTitle: () => (
            <Image
              source={require('../../assets/images/logo.png')}
              style={StackStyles.imageStyle}
            />
          ),
          headerTitleAlign: 'center',
        }}
      />
    </SettingStack.Navigator>
  );
}

const Styles = StyleSheet.create({
  drawer: {
    marginLeft: 10,
  },
  imageStyle: {
    width: 80,
    height: 30,
  },
});
